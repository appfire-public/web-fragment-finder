This plugin will allow you to generate a plugin from your JIRA installation that will show you the web sections, web items and web panels used in your instance. Please note the word usage: "used". This plugin scans the plugin system for declarations of web-items, web-sections and web-panels and adds call outs to their placements. The host app may have others that are not used at all. Note: This plugin does not rescan for new plugins - it can be done however due to the race condition possibility it was removed.

DO NOT INSTALL THIS INTO A PRODUCTION INSTANCE!
===============================================
This is for development instances only.
While nothing bad should happen - it will just get confusing for the end users.

To use:
-------
1. Install the plugin in instances that were started with atlas-run
2. Add ?web.items&web.sections&web.panels to the query string.

In some cases the host app will strip out the query string - in these cases - simple go to the Configuration page (Available through the "Hidden Webfragments" link in the top bar) and turn them on.

Web Resource Contexts
---------------------
For JIRA and Bamboo - in order to see the web-resource context's used on a page - you have to use the settings on the Hidden Webfragments page. For Confluence - simply add web.resources to the query string.

To uninstall:
-------------
This plugin actually generates a second plugin and installs it. In order to remove the plugin - remove BOTH of the plugins.

Tracking data
-------------
As of the 1.6 version we are are submitting 2 events to Google Analytics. The first event is upon start of the plugin. The second event is submitted every 30mins. Both of these events do not gather any data about your system or what you're doing. They're purely for us to gather usage statistics about the Web Fragment Finder. To disable this add -Dwebfrag.allow.google.tracking=true to your tomcat instance

Inspectors
==========
In the 2.0 release, a wide range of "inspectors" were introduced.
## Active Objects ##
The active objects inspector allows you to view all of the p2 that utilize Active Objects. From here you can select the add-on and then view both the java code AND the database content and structure of the table. 

**Note:** Some tables may not be viewed due to how the symbolic name is created by the system. This is usually the exception rather than the rule. If you encounter this, please file an issue in the Web Fragment issue tracker on Bitbucket.

Events
------
The event inspector allows you to see the events that have been triggered in the system since Web Fragment Finder was triggered. The saving of the events if batched to every 5minutes and can be forced by clicking the "Refresh" button.

HTTP
----
The HTTP inspector shows both the outbound and inbound connections. There might be up to 5mins delay for something to appear for performance reasons. However this can be forced by clicking on "Refresh".

The outbound connections relies on installing a http proxy within Web Fragment Finder and then reconfigures the instance to make use of it.

Properties
----------

The properties inspector is a wrapper around the rest objects available through JIRA and Confluence and allows you to view, edit, create and delete the various inspectors. Currently supported is Dashboard and Issue properties for JIRA and Content properties for Confluence.

Atlassian Connect support
=========================
With 2.0 there is a fair bit of Atlassian Connect support added. This includes the click to copy for atlassian connect son format.

**Atlassian Connect support is only available for JIRA and Confluence**

## Atlassian Connect plugin installation ##
With 2.0 you can install Atlassian Connect from within JIRA and Confluence from the configuration page. Simply click "Install Atlassian Connect" from the WebFrag configuration screen. This does require an active internet connection and will download the files from maven.atlassian.com to a .webfrags folder in your home directory for caching purposes.

Every 10minutes the add-on will poll connect.atlassian.com to see if the add ons need updating and if they do will alert you through the UI.

## Managed addons ##
By pasting in the full directory path to the atlassian-connect.json descriptor file and selecting "Update descriptor" on the main WebFrags configuration page, the descriptor will become a managed descriptor. This will mean that WebFrags will prompt you with a wizard whenever you click on "Add Web-Item" or click on a web panel. To edit a current web paitemnel, click on the web item link.

Once the form is submitted, WebFrags will update the descriptor.
**Note:** The Wizard does not show all options however does show the most common items.

## Node.js support ##
WebFrags will detect if the atlassian-connect.json file is in a node.js format add-on. If it is, it will offer to start the add-on for you. 

License:
========

Copyright (c) 2015, Wittified, LLC
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
    * Neither the name of the Wittified, LLC nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
