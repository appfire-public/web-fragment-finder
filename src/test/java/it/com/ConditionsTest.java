package it.com;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.conditions.ACHostAppUpdateCondition;
import com.wittified.fragfinder.conditions.ACHostPluginUpdateCondition;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;

/**
 * Created by daniel on 2/22/15.
 */

@RunWith(AtlassianPluginsTestRunner.class)
public class ConditionsTest
{

    private final PluginSettingsFactory pluginSettingsFactory;

    public ConditionsTest( final PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }


    @Test
    public void testIfTheACHostIsUpdated()
    {

        ACHostAppUpdateCondition condition = new ACHostAppUpdateCondition( this.pluginSettingsFactory);

        Assert.assertFalse(condition.shouldDisplay(new HashMap<String, Object>()));
        this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.ac.updateHost", "false");
        Assert.assertFalse(condition.shouldDisplay(new HashMap<String, Object>()));
        this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.ac.updateHost", "true");
        Assert.assertTrue(condition.shouldDisplay(new HashMap<String, Object>()));
    }

    public void testIfTheACPluginsAreUpdated()
    {

        ACHostPluginUpdateCondition condition = new ACHostPluginUpdateCondition( this.pluginSettingsFactory);

        Assert.assertFalse(condition.shouldDisplay(new HashMap<String, Object>()));
        this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.ac.updatePlugins", "false");
        Assert.assertFalse(condition.shouldDisplay(new HashMap<String, Object>()));
        this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.ac.updatePlugins", "true");
        Assert.assertTrue(condition.shouldDisplay(new HashMap<String, Object>()));
    }

}
