package it.com;

import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.wittified.fragfinder.ao.CapturedEvent;
import com.wittified.fragfinder.ao.CapturedEventManager;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.HashMap;

/**
 * Created by daniel on 2/22/15.
 */
@RunWith(AtlassianPluginsTestRunner.class)
public class CapturedEventManagerTest
{

    private final CapturedEventManager capturedEventManager;

    public CapturedEventManagerTest( final CapturedEventManager capturedEventManager )
    {
        this.capturedEventManager = capturedEventManager;
    }



    @Test
    public void testRegisteringAndFetchingEvents()
    {
        this.capturedEventManager.resetEvents();
        this.capturedEventManager.registerEvent( new Date(), "my event", new HashMap<String, String>());
        Assert.assertEquals( 1, this.capturedEventManager.getEvents().size() );
        Assert.assertEquals( "my event", this.capturedEventManager.getEvents().get(0).getName());
    }

    @Test
    public void testPaginationOfEvents()
    {
        this.capturedEventManager.resetEvents();
        for(int i=0;i<200;i++)
        {
            this.capturedEventManager.registerEvent( new Date(), "my event "+i, new HashMap<String, String>());
        }

        Assert.assertEquals(2, this.capturedEventManager.getEvents(2,3,"","") );
        Assert.assertEquals(2, this.capturedEventManager.getEvents(2,3,"","").getEvents().size()==2 );
        Assert.assertEquals(true, this.capturedEventManager.getEvents(2,3,"","").getNext() );

    }

    @Test
    public void testIncomingRegistration()
    {
        this.capturedEventManager.resetHttp();
        this.capturedEventManager.registerHTTP( new Date(), "http","foo","/","GET","", new HashMap<String, String>(), "");

        Assert.assertEquals( 1, this.capturedEventManager.getEvents().size() );
        Assert.assertEquals( "/", this.capturedEventManager.getHTTP(true, true).get(0).getPath());

    }


    @Test
    public void testOutgoingRegistration()
    {
        this.capturedEventManager.resetHttp();
        this.capturedEventManager.registerOutHTTP( new Date(), "http","foo","/","GET","", new HashMap<String, String>(),"", new HashMap<String, String>(),"" );

        Assert.assertEquals(1, this.capturedEventManager.getEvents().size());
        Assert.assertEquals( "/", this.capturedEventManager.getHTTP(true, true).get(0).getPath());

    }



    @Test
    public void getchingHttp()
    {
        this.capturedEventManager.resetHttp();
        for(int i=0;i<200;i++)
        {
            this.capturedEventManager.registerOutHTTP( new Date(), "http","foo","/"+i,"GET","", new HashMap<String, String>(),"", new HashMap<String, String>(),"" );
            capturedEventManager.registerHTTP(new Date(), "http", "foo", "/" + i, "GET", "", new HashMap<String, String>(), "");
        }

        Assert.assertEquals( 400, this.capturedEventManager.getHTTP(true, true).size());

    }

    @Test
    public void testContentExpiration()
    {
        this.capturedEventManager.resetEvents();
        this.capturedEventManager.resetHttp();
        Date oldDate = new Date( System.currentTimeMillis()-(60*60*24*1000*180));

        for(int i=0;i<200;i++)
        {
            this.capturedEventManager.registerEvent( oldDate, "my event "+i, new HashMap<String, String>());
            this.capturedEventManager.registerOutHTTP( oldDate, "http","foo","/"+i,"GET","", new HashMap<String, String>(),"", new HashMap<String, String>(),"" );
            this.capturedEventManager.registerHTTP( oldDate, "http", "foo", "/" + i, "GET", "", new HashMap<String, String>(), "");
        }


        for(int i=0;i<200;i++)
        {
            this.capturedEventManager.registerEvent( new Date(), "my new event "+i, new HashMap<String, String>());
            this.capturedEventManager.registerOutHTTP( new Date(), "http","foo","/new/"+i,"GET","", new HashMap<String, String>(),"", new HashMap<String, String>(),"" );
            this.capturedEventManager.registerHTTP(new Date(), "http", "foo", "/new/" + i, "GET", "", new HashMap<String, String>(), "");
        }

        this.capturedEventManager.expireContent();

        Assert.assertEquals( 400, this.capturedEventManager.getHTTP(true, true).size());
        Assert.assertEquals( 200, this.capturedEventManager.getEvents().size());

    }
}
