package com.wittified.fragfinder.service;

import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.Constants;

/**
 * Created by daniel on 3/6/15.
 */
public class ConfigurationServiceImpl implements ConfigurationService
{
    private final PluginSettingsFactory pluginSettingsFactory;

    public ConfigurationServiceImpl( final PluginSettingsFactory pluginSettingsFactory)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
    }

    @Override
    public boolean isHttpProxyEnabled()
    {

        String value = (String) this.pluginSettingsFactory.createGlobalSettings().get(Constants.PROXY_ENABLED_KEY);
        if( value!=null)
        {
            return Boolean.parseBoolean( value);
        }

        return false;
    }

    @Override
    public void setHttpProxyEnabled(boolean enabled)
    {
        this.pluginSettingsFactory.createGlobalSettings().put( Constants.PROXY_ENABLED_KEY, Boolean.toString( enabled));
    }
}
