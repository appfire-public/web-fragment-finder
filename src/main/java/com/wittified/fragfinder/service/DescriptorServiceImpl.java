package com.wittified.fragfinder.service;

import com.atlassian.gadgets.spec.Feature;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.PluginController;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.rest.entities.descriptor.*;
import com.wittified.fragfinder.servlets.PluginConfigurationServlet;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * Created by daniel on 11/3/14.
 */
public class DescriptorServiceImpl implements DescriptorService
{
    private final static Logger logger = LoggerFactory.getLogger( DescriptorServiceImpl.class);
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ACERunner aceRunner;

    public DescriptorServiceImpl( final PluginSettingsFactory pluginSettingsFactory,
                                  final ACERunner aceRunner)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.aceRunner = aceRunner;

    }

    public boolean doesDescriptorExist()
    {
        String path =(String) this.pluginSettingsFactory.createGlobalSettings().get(  PluginConfigurationServlet.COPY_PASTE_MANAGED_PATH );
        if( path==null )
        {
            return false;
        }
        if (new File( path).exists())
        {
            return true;
        }
        return false;
    }

    public boolean createDescriptor(String path)
    {
        if( doesDescriptorExist())
        {
            return false;
        }
        DescriptorEntity descriptorEntity = new DescriptorEntity();
        descriptorEntity.setModules(new ModuleEntities());
        descriptorEntity.setName("Sample addon");
        descriptorEntity.setBaseUrl("{{localBaseUrl}}");
        descriptorEntity.setKey("my-add-on");
        descriptorEntity.setDescription("My very first addon");
        descriptorEntity.setVersion("1.0");

        descriptorEntity.setEnableLicensing(false);
        LifeCycleEntity lifeCycleEntity = new LifeCycleEntity();
        lifeCycleEntity.setInstalled("/installed");

        VendorEntity vendorEntity = new VendorEntity();
        vendorEntity.setName("Angry Nerds");
        vendorEntity.setUrl("https://www.atlassian.com/angrynerds");
        descriptorEntity.setVendor( vendorEntity);

        LinksEntity linksEntity = new LinksEntity();
        linksEntity.setSelf("{{localBaseUrl}}/atlassian-connect.json");
        linksEntity.setHomepage("{{localBaseUrl}}/atlassian-connect.json");

        AuthenticationEntity authenticationEntity = new AuthenticationEntity();
        authenticationEntity.setType( "jwt ");
        descriptorEntity.setAuthentication( authenticationEntity);

        descriptorEntity.setLifecycle(lifeCycleEntity);
        descriptorEntity.setLinks( linksEntity);


        List<String> scopes = new ArrayList<String>();
        scopes.add("read");
        descriptorEntity.setScopes( scopes);

        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);


        try {
            mapper.writeValue(new File(path), descriptorEntity);
        }catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }
        return true;

    }
    public void saveDescriptorPath( String path)
    {
        this.pluginSettingsFactory.createGlobalSettings().put(  PluginConfigurationServlet.COPY_PASTE_MANAGED_PATH, path);
    }



    public Set<String> getAllKeys()
    {
        Map<String, String> keys = new HashMap<String, String>();

        DescriptorEntity descriptorEntity = this.getDescriptor();
        if( (descriptorEntity!=null) && ( descriptorEntity.getModules()!=null ) )
        {
            if( descriptorEntity.getModules().getWebItems()!=null)
            {
                for( WebItemEntity webItemEntity: descriptorEntity.getModules().getWebItems()){ keys.put( webItemEntity.getKey(), webItemEntity.getKey());}
            }
            if( descriptorEntity.getModules().getWebPanels()!=null)
            {
                for( WebPanelEntity webPanelEntity: descriptorEntity.getModules().getWebPanels()){ keys.put( webPanelEntity.getKey(), webPanelEntity.getKey());}
            }
            if( descriptorEntity.getModules().getAdminPages()!=null)
            {
                for( PageEntity pageEntity: descriptorEntity.getModules().getAdminPages()){ keys.put( pageEntity.getKey(), pageEntity.getKey());}
            }
            if( descriptorEntity.getModules().getGeneralPages()!=null)
            {
                for( PageEntity pageEntity: descriptorEntity.getModules().getGeneralPages()){ keys.put( pageEntity.getKey(), pageEntity.getKey());}
            }
        }


        return keys.keySet();
    }

    public void refresh() throws IOException
    {
        String key = this.getDescriptor().getKey();
        logger.debug("Looking for " + key);
    }



    public boolean addAdminPage( PageEntity adminPageEntity)
    {

        boolean restartServer = this.aceRunner.isCurrentlyRunning();
        if( restartServer)
        {
            this.aceRunner.stop();
        }
        DescriptorEntity descriptorEntity = this.getDescriptor();
        if( descriptorEntity==null) { return false;}

        if( descriptorEntity.getModules()==null)
        {
            descriptorEntity.setModules( new ModuleEntities());
        }

        List<PageEntity> pages = new ArrayList<PageEntity>();
        if( descriptorEntity.getModules().getAdminPages()!=null)
        {
            for(PageEntity oldPage: descriptorEntity.getModules().getAdminPages())
            {
                if( !oldPage.getKey().equals(adminPageEntity.getKey()))
                {
                    pages.add(oldPage);
                }
            }
        }
        pages.add( adminPageEntity);


        ModuleEntities moduleEntities = descriptorEntity.getModules();
        moduleEntities.setAdminPages( pages);
        descriptorEntity.setModules( moduleEntities);


        if( this.saveDescriptor( descriptorEntity))
        {
            if( restartServer && !this.aceRunner.isCurrentlyRunning())
            {
                this.aceRunner.start();
            }
            return true;
        }
        return false;

    }

    public boolean addGeneralPage( PageEntity generalPage)
    {
        boolean restartServer = this.aceRunner.isCurrentlyRunning();
        if( restartServer)
        {
            this.aceRunner.stop();
        }


        DescriptorEntity descriptorEntity = this.getDescriptor();
        if( descriptorEntity==null) { return false;}

        if( descriptorEntity.getModules()==null)
        {
            descriptorEntity.setModules( new ModuleEntities());
        }
        List<PageEntity> pages = new ArrayList<PageEntity>();

        if( descriptorEntity.getModules().getGeneralPages()!=null)
        {
            for(PageEntity oldPage: descriptorEntity.getModules().getGeneralPages())
            {
                if( !oldPage.getKey().equals(generalPage.getKey()))
                {
                    pages.add(oldPage);
                }
            }
        }
        pages.add( generalPage);


        ModuleEntities moduleEntities = descriptorEntity.getModules();
        moduleEntities.setGeneralPages( pages);
        descriptorEntity.setModules( moduleEntities);

        if( this.saveDescriptor( descriptorEntity))
        {
            if( restartServer && !this.aceRunner.isCurrentlyRunning())
            {
                this.aceRunner.start();
            }
            return true;
        }
        return false;
    }

    public boolean addWebItem( WebItemEntity webItemEntity)
    {

        boolean restartServer = this.aceRunner.isCurrentlyRunning();
        if( restartServer)
        {
            this.aceRunner.stop();
        }

        DescriptorEntity descriptorEntity = this.getDescriptor();
        if( descriptorEntity==null) { return false;}

        if( descriptorEntity.getModules()==null)
        {
            descriptorEntity.setModules( new ModuleEntities());
        }

        List<WebItemEntity> webItems = new ArrayList<WebItemEntity>();
        if( descriptorEntity.getModules().getWebItems()!=null)
        {
            for(WebItemEntity oldWebItem: descriptorEntity.getModules().getWebItems())
            {
                if( !oldWebItem.getKey().equals(webItemEntity.getKey()))
                {
                    webItems.add(oldWebItem);
                }
            }
        }
        webItems.add( webItemEntity);


        ModuleEntities moduleEntities = descriptorEntity.getModules();
        moduleEntities.setWebItems( webItems);
        descriptorEntity.setModules( moduleEntities);



        descriptorEntity.getModules().setWebItems( webItems);


        if( this.saveDescriptor( descriptorEntity))
        {
            if( restartServer && !this.aceRunner.isCurrentlyRunning())
            {
                this.aceRunner.start();
            }
            return true;
        }
        return false;

    }

    public boolean addWebPanel( WebPanelEntity webPanelEntity)
    {
        boolean restartServer = this.aceRunner.isCurrentlyRunning();
        if( restartServer)
        {
            this.aceRunner.stop();
        }


        if( this.getAllKeys().contains( webPanelEntity.getKey()))
        {
            return false;
        }

        DescriptorEntity descriptorEntity = this.getDescriptor();
        if( descriptorEntity==null) { return false;}

        if( descriptorEntity.getModules()==null)
        {
            descriptorEntity.setModules( new ModuleEntities());
        }

        List<WebPanelEntity> webPanelEntities = new ArrayList<WebPanelEntity>();

        if(descriptorEntity.getModules().getWebPanels()!=null) {

            for (WebPanelEntity existingWebPanel : descriptorEntity.getModules().getWebPanels()) {
                if (!existingWebPanel.getKey().equals(webPanelEntity.getKey())) {
                    webPanelEntities.add(existingWebPanel);
                }
            }
        }
        webPanelEntities.add( webPanelEntity);

        ModuleEntities moduleEntities = descriptorEntity.getModules();
        moduleEntities.setWebPanels( webPanelEntities);
        descriptorEntity.setModules( moduleEntities);



        if( this.saveDescriptor( descriptorEntity))
        {
            if( restartServer && !this.aceRunner.isCurrentlyRunning())
            {
                this.aceRunner.start();
            }
            return true;
        }
        return false;

    }

    public Object getModule(String key)
    {
        DescriptorEntity descriptorEntity = this.getDescriptor();
        if( descriptorEntity!=null)
        {
            ModuleEntities moduleEntities = descriptorEntity.getModules();
            Object pageEntity = this.findItem( moduleEntities.getGeneralPages(), key);
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getAdminPages(), key);}
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getJiraComponentTabPanels(), key);}
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getJiraProjectAdminTabPanels(), key);}
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getJiraProjectTabPanels(), key);}
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getJiraSearchRequestViews(), key);}
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getJiraVersionTabPanels(), key);}
            if( pageEntity==null) { pageEntity = this.findItem( moduleEntities.getProfilePages(), key);}
            if( pageEntity==null && moduleEntities.getConfigurePage()!=null && moduleEntities.getConfigurePage().getKey().equals( key))
            {
                pageEntity = moduleEntities.getConfigurePage();
            }

            if( pageEntity==null &&( moduleEntities.getWebItems()!=null)) {

                for (WebItemEntity webItemEntity : moduleEntities.getWebItems()) {
                    if (webItemEntity.getKey().equals(key)) {
                        pageEntity = webItemEntity;
                    }
                }
            }

            if( pageEntity==null && ( moduleEntities.getWebPanels()!=null)) {
                for (WebPanelEntity webPanelEntity : moduleEntities.getWebPanels()) {
                    if (webPanelEntity.getKey().equals(key)) {
                        pageEntity = webPanelEntity;
                    }
                }
            }

            return pageEntity;
        }

        return null;
    }


    public boolean isAdminPage (PageEntity entity)
    {
        ModuleEntities moduleEntities = this.getDescriptor().getModules();
        if( moduleEntities!=null)
        {
            if( moduleEntities.getAdminPages()!=null)
            {
                return ( this.findItem( moduleEntities.getAdminPages(), entity.getKey())!=null);
            }
        }
        return false;
    }
    public boolean isGeneralPage (PageEntity entity)
    {
        ModuleEntities moduleEntities = this.getDescriptor().getModules();
        if( moduleEntities!=null)
        {
            if( moduleEntities.getGeneralPages()!=null)
            {
                return ( this.findItem( moduleEntities.getGeneralPages(), entity.getKey())!=null);
            }
        }
        return false;
    }

    private Object findItem( List<PageEntity> pageEntities, String key)
    {
        if( pageEntities==null) { return null;}
        for( PageEntity pageEntity: pageEntities )
        {
            if( pageEntity.getKey().equals(key))
            {
                return pageEntity;
            }
        }
        return null;
    }

    public DescriptorEntity getDescriptor()
    {
        try {
            String path = (String) this.pluginSettingsFactory.createGlobalSettings().get(PluginConfigurationServlet.COPY_PASTE_MANAGED_PATH);
            if(path==null)
            {
                return null;
            }

            ObjectMapper mapper = new ObjectMapper();

            DescriptorEntity descriptorEntity = mapper.readValue(new File(path), DescriptorEntity.class);

            mapper=null;
            return descriptorEntity;
        }
        catch(IOException e)
        {
            e.printStackTrace();
        }
        return null;
    }

    private boolean saveDescriptor( DescriptorEntity descriptorEntity)
    {
        ObjectMapper mapper = new ObjectMapper();


        String path =(String) this.pluginSettingsFactory.createGlobalSettings().get(  PluginConfigurationServlet.COPY_PASTE_MANAGED_PATH);

        mapper.setSerializationInclusion(JsonSerialize.Inclusion.ALWAYS);
        mapper.enable(SerializationConfig.Feature.INDENT_OUTPUT);



        File finalFile =  new File(path);
        try
        {
            System.err.println("Saving " + mapper.writeValueAsString(descriptorEntity));

            File tempFile = File.createTempFile("webfrags",".json");
            FileWriter fileWriter = new FileWriter(tempFile);

            fileWriter.write( mapper.writeValueAsString(descriptorEntity) );
            fileWriter.flush();
            fileWriter.close();
            tempFile.renameTo( finalFile);

            this.refresh();

        }catch(IOException e)
        {
            e.printStackTrace();
            return false;
        }

        return true;

    }

}
