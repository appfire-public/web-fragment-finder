package com.wittified.fragfinder.service;

import com.atlassian.plugin.*;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.Constants;
import com.wittified.fragfinder.jobs.ACUpdateService;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by daniel on 12/5/14.
 */
public class VersionServiceImpl implements VersionService
{
    private final ApplicationProperties applicationProperties;
    private final RequestFactory requestFactory;
    private final PluginAccessor pluginAccessor;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final PluginController pluginController;

    private static final Logger logger = LoggerFactory.getLogger(VersionServiceImpl.class);

    private final Pattern versionExtractor = Pattern.compile(".+--bundled-plugins ([^\\s]+).+");
    private final String jsonURL = "https://developer.atlassian.com/static/connect-versions.json";
    private final String MAVEN_BASE = "https://maven.atlassian.com/repository/public/";

    public VersionServiceImpl( final ApplicationProperties applicationProperties,
                               final RequestFactory requestFactory,
                               final PluginAccessor pluginAccessor,
                               final PluginController pluginController,
                               final PluginSettingsFactory pluginSettingsFactory)
    {
        this.applicationProperties = applicationProperties;
        this.requestFactory = requestFactory;
        this.pluginAccessor = pluginAccessor;
        this.pluginController = pluginController;
        this.pluginSettingsFactory = pluginSettingsFactory;
    }



    public boolean isHostedAppUpToDate( String env )
    {
        return this.applicationProperties.getVersion().equals( this.getCurrentPublishedVersion( env));
    }
    public boolean arePluginsUpToDate( String env )
    {
        return this.getPluginsNeedingUpdate(env).size()==0;
    }

    public String getCurrentPublishedVersion( String env )
    {
        JsonNode data = this.getData(env);
        String currentApp = null;
        if( this.applicationProperties.getDisplayName().equalsIgnoreCase("JIRA"))
        {
            currentApp = "jiraCommand";
        }
        else if( this.applicationProperties.getDisplayName().equalsIgnoreCase("Confluence"))
        {
            currentApp = "confluenceCommand";
        }

        if( currentApp!=null && data!=null && data.get(currentApp)!=null)
        {
            String command =  data.get(currentApp).asText();
            if( command.contains("--version"))
            {
                int startAt = command.indexOf("--version")+10;
                int endAt = command.indexOf(" ", startAt);
                return command.substring(startAt, endAt);
            }
        }
        return null;
    }

    public Map<String, Map<String, String>> getPluginsNeedingUpdate( String env )
    {
        Map<String, Map<String, String>> pluginMap = new LinkedHashMap<String, Map<String, String>>();


        JsonNode data = this.getData(env);


        String currentApp = null;
        if( this.applicationProperties.getDisplayName().equalsIgnoreCase("JIRA"))
        {
            currentApp = "jiraCommand";
        }
        else if( this.applicationProperties.getDisplayName().equalsIgnoreCase("Confluence"))
        {
            currentApp = "confluenceCommand";
        }

        if( currentApp!=null && data!=null && data.has(currentApp))
        {

            String command = data.get( currentApp).getTextValue();
            Matcher matcher = this.versionExtractor.matcher( command);
            if( matcher.matches())
            {
                String pluginBundle = matcher.group(1);
                for(String pluginStr : pluginBundle.split(","))
                {
                    String[] mavenCoords = pluginStr.split(":");
                    if( mavenCoords.length>1)
                    {
                        String packageStr = mavenCoords[0];
                        String artifactStr = mavenCoords[1];
                        String versionStr = mavenCoords[2];

                        Map<String, String> artifactData = new WeakHashMap<String, String>();
                        artifactData.put("pluginId", packageStr+"."+ artifactStr);
                        artifactData.put("version", versionStr);
                        artifactData.put("package", packageStr);
                        artifactData.put("artifact", artifactStr);


                        try {
                            URL u = this.resolveArtifact(packageStr, artifactStr, versionStr);
                            if(u==null)
                            {
                                System.err.println("Unable to resolve "+ pluginStr);
                            }
                            else {

                                artifactData.put("url", u.toString());
                                if (artifactData.get("url").endsWith(".obr")) {
                                    artifactData.put("extension", "obr");
                                } else {
                                    artifactData.put("extension", "jar");
                                }
                            }

                        }
                        catch(IOException e)
                        {
                            e.printStackTrace();

                        }
                        // check to see if this is a jar or an obr


                        String pluginId = packageStr+"."+ artifactStr;
                        Plugin thePlugin = this.pluginAccessor.getPlugin( pluginId);
                        if( thePlugin==null)
                        {
                            pluginMap.put( pluginId, artifactData);
                        }
                        else
                        {
                            if( !thePlugin.getPluginInformation().getVersion().equals( versionStr))
                            {
                                pluginMap.put( pluginId, artifactData);
                            }
                        }
                    }
                }
            }


        }


        return pluginMap;
    }

    private PluginUpgradeThread upgradeThread = null;
    public boolean updatePlugins( String env)
    {
        LinkedHashMap<String, Map<String, String>> pluginData = (LinkedHashMap) this.getPluginsNeedingUpdate( env);

        if(upgradeThread==null || upgradeThread.isCompleted())
        {
            this.upgradeThread = new PluginUpgradeThread();
            this.upgradeThread.setPluginController(this.pluginController);
            this.upgradeThread.setPluginData(pluginData);
            this.upgradeThread.setRequestFactory(this.requestFactory);
            this.upgradeThread.run();

            return true;
        }

        return false;
    }


    public boolean isPluginUpgradeInProgress()
    {
        if( this.upgradeThread==null)
        {
            return false;
        }

        if( this.upgradeThread.isCompleted())
        {
            this.pluginSettingsFactory.createGlobalSettings().remove(Constants.SETTINGS_UPDATE_PLUGIN);
            return false;
        }

        return ( this.upgradeThread.isHasStarted());
    }


    private JsonNode getData( String env)
    {
        if(env==null) { return null;}
        Request theRequest = this.requestFactory.createRequest(Request.MethodType.GET, jsonURL);
        String json = null;
        try {
            json = theRequest.execute();
        }
        catch(ResponseException r )
        {
            r.printStackTrace();
            return null;
        }

        ObjectMapper objectMapper = new ObjectMapper();

        logger.debug( json);

        try
        {
            JsonNode rootNode = objectMapper.readTree(json);
            if(rootNode!=null)
            {
                logger.debug( rootNode.has("environment")+"");
                if( rootNode.has("environment"))
                {
                    JsonNode envNode = rootNode.get("environment");
                    logger.debug("Looking for "+ env);
                    if( envNode.has( env.trim()))
                    {
                        logger.debug("Returning "+ envNode.get( env));
                        return envNode.get( env);
                    }
                }
            }

        }
        catch(IOException io)
        {
            io.printStackTrace();
        }
        return null;
    }


    private URL resolveArtifact( String groupId, String packageId, String versionId) throws IOException
    {
        File pluginDir = new File(new File( new File(  System.getProperty("user.home") ),".webfrags"),"plugins") ;
        if( !pluginDir.exists()) {
            if( !pluginDir.mkdirs() )
            {
                pluginDir = null;
            }
        }
        File artifactFile = null;
        if(pluginDir!=null)
        {
            artifactFile = new File( pluginDir, groupId+"."+packageId+"-"+versionId+".obr");
            if( !artifactFile.exists())
            {
                artifactFile = new File( pluginDir, groupId+"."+packageId+"-"+versionId+".jar");
            }
        }
        if ((artifactFile!=null ) && (artifactFile.exists()))
        {
            return artifactFile.toURI().toURL();
        }


        String url = null;
        String jarUrl = MAVEN_BASE+(groupId.replaceAll("\\.","/"))+"/"+packageId+"/"+versionId+"/"+packageId+"-"+versionId+".jar";
        String obrUrl = MAVEN_BASE+(groupId.replaceAll("\\.","/"))+"/"+packageId+"/"+versionId+"/"+packageId+"-"+versionId+".obr";


        try {
            this.requestFactory.createRequest(Request.MethodType.HEAD, jarUrl).execute();
            url = jarUrl;

        }
        catch(ResponseException e)
        {
            e.printStackTrace();
        }
        if(url==null)
        {
            try {
                this.requestFactory.createRequest(Request.MethodType.HEAD, obrUrl).execute();
                url = obrUrl;
            }
            catch(ResponseException e)
            {
                e.printStackTrace();
            }

        }
        if(url == null)
        {
            return null;
        }

        return new URL( url);
    }

}
