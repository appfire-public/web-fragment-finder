package com.wittified.fragfinder.service;

import com.atlassian.plugin.StateAware;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.rest.entities.descriptor.DescriptorEntity;

import java.util.*;

/**
 * Created by daniel on 3/29/15.
 */
public class ACEEditorServiceImpl implements ACEEditorService, LifecycleAware
{
    private Set<String> keys = null;
    String currentWatchedKey = null;

    private final DescriptorService descriptorService;

    public ACEEditorServiceImpl( final DescriptorService descriptorService)
    {
        this.descriptorService = descriptorService;
    }

    @Override
    public void onStart() {
        this.reload();
    }

    @Override
    public void onStop() {
            this.keys=null;
    }

    public void reload()
    {

        DescriptorEntity descriptor = this.descriptorService.getDescriptor();
        this.keys = new HashSet<String>();
        if( descriptor!=null) {
            this.currentWatchedKey = descriptor.getKey();
            for(String k: this.descriptorService.getAllKeys())
            {
                System.err.println("Adding "+ k);
                this.keys.add( this.currentWatchedKey+"__"+k);
            }
        }
    }

    public String removePluginKey( String key)
    {
        if( key.startsWith( this.currentWatchedKey))
        {
            return key.substring(this.currentWatchedKey.length()+2);

        }
        return key;
    }

    public String getCurrentPluginKey()
    {
        return this.currentWatchedKey;
    }

    public List<String> getKeys()
    {
        return new ArrayList<String>( this.keys);
    }
}
