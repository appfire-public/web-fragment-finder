package com.wittified.fragfinder.service;

import com.atlassian.plugin.StateAware;
import io.netty.handler.codec.http.*;
import org.littleshoot.proxy.HttpFilters;
import org.littleshoot.proxy.HttpFiltersAdapter;
import org.littleshoot.proxy.HttpFiltersSourceAdapter;
import org.littleshoot.proxy.HttpProxyServer;
import org.littleshoot.proxy.impl.DefaultHttpProxyServer;
import org.littleshoot.proxy.impl.ProxyUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * Created by daniel on 3/5/15.
 */
public class HttpProxyServiceImpl implements HttpProxyService, DisposableBean
{
    private HttpProxyServer server = null;
    private final DataTracker dataTracker;

    private final static Logger logger = LoggerFactory.getLogger( HttpProxyServiceImpl.class);

    public HttpProxyServiceImpl(final DataTracker dataTracker)
    {
        this.dataTracker = dataTracker;
    }

    @Override
    public boolean start()
    {



        HttpFiltersSourceAdapter adapter = new HttpFiltersSourceAdapter()
        {
            @Override
            public HttpFilters filterRequest(HttpRequest originalRequest) {

                return new HttpFiltersAdapter( originalRequest)
                {
                    HttpRequest theRequest = originalRequest;


                    String host =  ProxyUtils.parseHostAndPort(originalRequest);
                    String method = originalRequest.getMethod().name();
                    String path = originalRequest.getUri();
                    String query = "";
                    StringBuffer content = new StringBuffer();
                    long startTime = System.currentTimeMillis();
                    String protocol = originalRequest.getProtocolVersion().protocolName();
                    Map<String, String> headers = new HashMap<String, String>();
                    Map<String, String> requestHeaders = new HashMap<String, String>();
                    String requestBody = "";

                    public HttpResponse requestPre(HttpObject httpObject)
                    {
                        if( httpObject instanceof HttpRequest) {
                            HttpRequest httpRequest = (HttpRequest) httpObject;

                            Iterator<Map.Entry<String, String>> iterator = httpRequest.headers().iterator();
                            while(iterator.hasNext())
                            {
                                Map.Entry<String, String> entry = iterator.next();
                                this.requestHeaders.put( entry.getKey(), entry.getValue() );

                            }

                        }
                        if(httpObject instanceof HttpContent)
                        {
                            requestBody = ((HttpContent) httpObject).content().toString( Charset.forName("UTF-8"));
                        }

                            return null;
                    }


                    public HttpResponse requestPost(HttpObject httpObject)
                    {
                        if( httpObject instanceof HttpRequest)
                        {
                            HttpRequest httpRequest = (HttpRequest) httpObject;

                            Iterator<Map.Entry<String, String>> iterator = httpRequest.headers().iterator();
                            while(iterator.hasNext())
                            {
                                Map.Entry<String, String> entry = iterator.next();
                                this.requestHeaders.put( entry.getKey(), entry.getValue() );

                            }

                            if( ProxyUtils.isCONNECT(httpObject))
                            {
                                dataTracker.storeOutHTTPData(this.protocol, this.host, this.path, this.method, headers, this.query, this.content.toString(), this.requestHeaders, this.requestBody);
                            }



                        }
                        if(httpObject instanceof HttpContent)
                        {
                            requestBody = ((HttpContent) httpObject).content().toString( Charset.forName("UTF-8"));
                        }
                        return null;
                    }


                    public HttpObject responsePre(HttpObject httpObject) {

                        return httpObject;
                    }


                    public HttpObject responsePost(HttpObject httpObject)
                    {

                        if (httpObject instanceof HttpResponse)
                        {
                            HttpResponse httpResponse = (HttpResponse) httpObject;

                            HttpHeaders httpHeaders = httpResponse.headers();
                            for(String headerName: httpHeaders.names())
                            {
                                this.headers.put( headerName, httpHeaders.get(headerName));
                            }

                        }
                        else if(httpObject instanceof HttpContent)
                        {
                            this.content.append ( ((HttpContent) httpObject).content().toString( Charset.forName("UTF-8")) );

                        }
                        else if( httpObject instanceof LastHttpContent)
                        {
                            this.content.append ( ((LastHttpContent) httpObject).content().toString( Charset.forName("UTF-8")) );

                        }
                        long totalTime = System.currentTimeMillis()-startTime;

                        logger.debug("Done after "+ totalTime);

                        if( !ProxyUtils.isCONNECT(httpObject))
                        {

                            if (ProxyUtils.isChunked(httpObject)) {
                                if (ProxyUtils.isLastChunk(httpObject)) {
                                    dataTracker.storeOutHTTPData(this.protocol, this.host, this.path, this.method, headers, this.query, this.content.toString(), this.requestHeaders, this.requestBody);
                                }

                            } else {
                                dataTracker.storeOutHTTPData(this.protocol, this.host, this.path, this.method, headers, this.query, this.content.toString(),  this.requestHeaders, this.requestBody);
                            }
                        }

                        return httpObject;

                    }


                };

            }
        };


        this.server =
                DefaultHttpProxyServer.bootstrap()
                        .withPort(8888)

                        .withFiltersSource( adapter)
                        .

                                start();

        logger.debug("Setting properties");
        System.setProperty("http.proxyHost",this.server.getListenAddress().

                        getHostName()

        );
        System.setProperty("http.proxyPort",this.server.getListenAddress().

                getPort()

                +"");
        System.setProperty("https.proxyHost",this.server.getListenAddress().

                        getHostName()

        );
        System.setProperty("https.proxyPort",this.server.getListenAddress().

                getPort()

                +"");

        return true;
    }

    @Override
    public void stop()
    {

        logger.info("Disabling proxy");
        if(this.server!=null)
        {
            this.server.stop();
            this.server = null;
        }
    }

    @Override
    public void destroy() throws Exception {
        logger.info("Destroying proxy");
        if(this.server!=null)
        {
            this.server.stop();
            this.server = null;
        }
    }
}
