package com.wittified.fragfinder.service;

import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public interface DataTracker
{
    public void storeEventData( String name, Map<String,String> params);

    public void storeHTTPData( String protocol, String server, String path, String method, Map<String, String> headers, String query, String body);
    public void storeOutHTTPData( String protocol, String server, String path,String method, Map<String, String> headers, String query, String content, Map<String, String> requestHeaders, String requestBody);
    public void transferData();
}
