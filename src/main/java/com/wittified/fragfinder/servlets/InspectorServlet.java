package com.wittified.fragfinder.servlets;

import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.wittified.fragfinder.service.DescriptorService;
import com.wittified.fragfinder.jobs.ACUpdateService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;


/**
 * Copyright (c) 2013, Wittified, LLC
 * All rights reserved.
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the Wittified, LLC nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


public class InspectorServlet extends HttpServlet
{
	private final PluginSettingsFactory pluginSettingsFactory;
	private final SoyTemplateRenderer soyTemplateRenderer;
    private final ApplicationProperties applicationProperties;
    private final WebResourceManager webResourceManager;
    private final DescriptorService descriptorService;
    private final UserManager userManager;
    private final ACUpdateService acUpdateService;
    private final LoginUriProvider loginUriProvider;



	public InspectorServlet(final PluginSettingsFactory pluginSettingsFactory,
                           final SoyTemplateRenderer soyTemplateRenderer,
                           final ApplicationProperties applicationProperties,
                           final WebResourceManager webResourceManager,
                           final DescriptorService descriptorService,
                           final UserManager userManager,
                           final ACUpdateService acUpdateService,
                           final LoginUriProvider loginUriProvider)
	{
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.soyTemplateRenderer = soyTemplateRenderer;
        this.applicationProperties = applicationProperties;
        this.webResourceManager = webResourceManager;
        this.descriptorService = descriptorService;
        this.userManager = userManager;
        this.acUpdateService = acUpdateService;
        this.loginUriProvider = loginUriProvider;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{

        String displayItem = "";
        if( req.getPathInfo().isEmpty())
        {
            resp.setStatus(404);
            return;
        }
        displayItem = req.getPathInfo().substring(1);

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {

            if( this.userManager.getRemoteUsername()==null)
            {
                try
                {
                    resp.sendRedirect( this.loginUriProvider.getLoginUri(new URI("/plugins/servlet/webfrag-finder/inspector/"+displayItem)).toString());
                    return;
                }catch(Exception e)
                {

                }
            }
            resp.setStatus(401);
            return;
        }
		Map<String, Object> data = new HashMap<String, Object>();

        boolean isJIRA = this.applicationProperties.getDisplayName().equalsIgnoreCase("JIRA");
        boolean isConfluence = this.applicationProperties.getDisplayName().equalsIgnoreCase("confluence");

        data.put("isJIRA", isJIRA );
        data.put("isConfluence", isConfluence );
        data.put("isStash", this.applicationProperties.getDisplayName().equalsIgnoreCase("stash"));
        data.put("displayItem", displayItem);
        data.put("contextPath", this.applicationProperties.getBaseUrl(UrlMode.ABSOLUTE));


        this.webResourceManager.requireResourcesForContext("webfrags-javascript-panel");
        if( isJIRA)
        {
            this.webResourceManager.requireResourcesForContext("webfrags-javascript-panel-jira");

        }
        else if  (isConfluence)
        {
            this.webResourceManager.requireResourcesForContext("webfrags-javascript-panel-confluence");

        }

        try
		{
			resp.setHeader("Content-Type", "text/html");
			resp.getWriter().write(this.soyTemplateRenderer.render("com.wittified.webfragment-finder:soy-templates-server",
																	   "com.wittified.fragfinder.displayBrowser",data));
		}
		catch(SoyException e)
		{
			e.printStackTrace();
		}

	}


}
