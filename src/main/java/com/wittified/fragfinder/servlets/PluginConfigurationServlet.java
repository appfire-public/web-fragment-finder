package com.wittified.fragfinder.servlets;

import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.UrlMode;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.wittified.fragfinder.service.ACEEditorService;
import com.wittified.fragfinder.service.DescriptorService;
import com.wittified.fragfinder.jobs.ACUpdateService;
import com.wittified.fragfinder.service.ACERunner;
import org.apache.commons.lang3.JavaVersion;
import org.apache.commons.lang3.SystemUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;


/**
 * Copyright (c) 2013, Wittified, LLC
 * All rights reserved.
 * <p/>
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 * Neither the name of the Wittified, LLC nor the
 * names of its contributors may be used to endorse or promote products
 * derived from this software without specific prior written permission.
 * <p/>
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


public class PluginConfigurationServlet extends HttpServlet
{

    private final static Logger logger = LoggerFactory.getLogger( PluginConfigurationServlet.class);

	private final PluginSettingsFactory pluginSettingsFactory;
	private final SoyTemplateRenderer soyTemplateRenderer;
    private final ApplicationProperties applicationProperties;
    private final WebResourceManager webResourceManager;
    private final DescriptorService descriptorService;
    private final UserManager userManager;
    private final ACUpdateService acUpdateService;
    private final ACERunner aceRunner;
    private final PluginAccessor pluginAccessor;
    private final ACEEditorService aceEditorService;
    private final LoginUriProvider loginUriProvider;

	public final static String ALWAYS_DISPLAY_WEB_RESOURCES = "com.wittified.plugin-generator.displayHiddenWebResources";
	public final static String ALWAYS_DISPLAY_WEB_ITEM = "com.wittified.plugin-generator.displayHiddenWebItem";
	public final static String ALWAYS_DISPLAY_WEB_SECTION = "com.wittified.plugin-generator.displayHiddenWebSection";
	public final static String ALWAYS_DISPLAY_WEB_PANEL = "com.wittified.plugin-generator.displayHiddenWebPanel";
    public final static String COPY_PASTE_FORMAT_KEY = "com.wittified.plugin-generator.copyFormat";
    public final static String COPY_PASTE_MANAGED_PATH = "com.wittified.plugin-generator.managedDescriptor";
    public final static String COPY_PASTE_FORMAT_AC = "ac";
    public final static String COPY_PASTE_FORMAT_P2 = "p2";
    public final static String COPY_PASTE_FORMAT_MANAGED_AC = "ac-descriptor";
    public final static String COPY_PASTE_FORMAT_NONE = "none";


	public PluginConfigurationServlet(final PluginSettingsFactory pluginSettingsFactory,
                                      final SoyTemplateRenderer soyTemplateRenderer,
                                      final ApplicationProperties applicationProperties,
                                      final WebResourceManager webResourceManager,
                                      final DescriptorService descriptorService,
                                      final UserManager userManager,
                                      final ACUpdateService acUpdateService,
                                      final ACERunner aceRunner,
                                      final PluginAccessor pluginAccessor,
                                      final LoginUriProvider loginUriProvider,
                                      final ACEEditorService aceEditorService)
	{
		this.pluginSettingsFactory = pluginSettingsFactory;
		this.soyTemplateRenderer = soyTemplateRenderer;
        this.applicationProperties = applicationProperties;
        this.webResourceManager = webResourceManager;
        this.descriptorService = descriptorService;
        this.userManager = userManager;
        this.acUpdateService = acUpdateService;
        this.aceRunner = aceRunner;
        this.pluginAccessor = pluginAccessor;
        this.loginUriProvider = loginUriProvider;
        this.aceEditorService = aceEditorService;
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
	{

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            if( this.userManager.getRemoteUsername()==null)
            {
                try
                {
                    resp.sendRedirect( this.loginUriProvider.getLoginUri(new URI("/plugins/servlet/webfrag-finder/config")).toString());
                    return;
                }catch(Exception e)
                {

                }
            }

            resp.setStatus(401);
            return;
        }

        boolean isJIRA = this.applicationProperties.getDisplayName().equalsIgnoreCase("JIRA");
        boolean isConfluence = this.applicationProperties.getDisplayName().equalsIgnoreCase("confluence");

        String copyPasteFormat =  this.getConfigValue(COPY_PASTE_FORMAT_KEY,"");
        if( copyPasteFormat.equals("") && this.checkForAC())
        {
            copyPasteFormat = COPY_PASTE_FORMAT_AC;
        }
		Map<String, Object> data = new HashMap<String, Object>();

        String descriptorPath = this.getConfigValue(COPY_PASTE_MANAGED_PATH, "");
		data.put("displayHiddenResources", this.getConfigValue(ALWAYS_DISPLAY_WEB_RESOURCES));
		data.put("displayHiddenPanel", this.getConfigValue(ALWAYS_DISPLAY_WEB_PANEL));
		data.put("displayHiddenItem", this.getConfigValue(ALWAYS_DISPLAY_WEB_ITEM));
        data.put("displayHiddenSection", this.getConfigValue(ALWAYS_DISPLAY_WEB_SECTION));
        data.put("copyPasteFormat", copyPasteFormat);
        data.put("descriptorPath", descriptorPath);
        data.put("isJIRA", isJIRA );
        data.put("descriptorDir","" );
        data.put("isACInstalled", this.checkForAC() );
        data.put("isConfluence", isConfluence);
        data.put("isStash", this.applicationProperties.getDisplayName().equalsIgnoreCase("stash"));
        data.put("acVersionEnabled", this.pluginSettingsFactory.createGlobalSettings().get("com.wittified.webfrags.acversion") );
        data.put( "contextPath", this.applicationProperties.getBaseUrl(UrlMode.ABSOLUTE));

        data.put("canDoAC", SystemUtils.isJavaVersionAtLeast(JavaVersion.JAVA_1_8));
        data.put( "isACEDescriptor",false);
        data.put( "isACERunning",false);
        if( isConfluence || isJIRA)
        {
            data.put("isACERunning", this.aceRunner.isCurrentlyRunning());
            if( descriptorPath.endsWith("atlassian-connect.json"))
            {
                File descriptorFile = new File(descriptorPath);
                File appDir = descriptorFile.getParentFile();
                logger.info("looking in " + appDir.getAbsolutePath());
                if( new File(appDir, "package.json").exists() && new File(appDir, "app.js").exists() && new File(appDir, "node_modules").exists())
                {
                    data.put("descriptorDir", appDir.getAbsoluteFile() );
                    data.put( "isACEDescriptor", true);
                }
            }
        }



        this.webResourceManager.requireResourcesForContext("webfrags-javascript-panel");

        try
		{
			resp.setHeader("Content-Type", "text/html");
			resp.getWriter().write(this.soyTemplateRenderer.render("com.wittified.webfragment-finder:soy-templates-server",
																	   "com.wittified.fragfinder.handleHiddenItem",data));
		}
		catch(SoyException e)
		{
			e.printStackTrace();
		}

	}


    private String getConfigValue( String key, String defaultValue)
    {
        Object item = this.pluginSettingsFactory.createGlobalSettings().get( key);
        if( item!=null)
        {
            return (String) item;
        }
        return defaultValue;
    }

	private Boolean getConfigValue( String key)
	{
		Object item = this.pluginSettingsFactory.createGlobalSettings().get( key );
		if(item!=null)
		{
			return Boolean.parseBoolean( (String) item);
		}
		return false;

	}


	@Override
	protected void doPost( HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            response.setStatus(401);
            return;
        }

		this.pluginSettingsFactory.createGlobalSettings().put(ALWAYS_DISPLAY_WEB_RESOURCES, request.getParameter("displayHiddenResources"));
		this.pluginSettingsFactory.createGlobalSettings().put(ALWAYS_DISPLAY_WEB_ITEM, request.getParameter("displayHiddenItem"));
		this.pluginSettingsFactory.createGlobalSettings().put(ALWAYS_DISPLAY_WEB_PANEL, request.getParameter("displayHiddenPanel"));
		this.pluginSettingsFactory.createGlobalSettings().put(ALWAYS_DISPLAY_WEB_SECTION, request.getParameter("displayHiddenSection"));
        this.pluginSettingsFactory.createGlobalSettings().put( COPY_PASTE_FORMAT_KEY, request.getParameter( "copyPasteFormat"));

        if( request.getParameter("copyPasteFormat")!=null && request.getParameter("copyPasteFormat").equals( COPY_PASTE_FORMAT_MANAGED_AC))
        {
            String path = request.getParameter( "descriptorPath");
            this.pluginSettingsFactory.createGlobalSettings().put(COPY_PASTE_MANAGED_PATH, path );
            if(!this.descriptorService.doesDescriptorExist())
            {
                this.descriptorService.createDescriptor( path);

            }
            this.aceEditorService.reload();
        }

        String versionTrackerVal = request.getParameter("acVersionTracker");
        if( versionTrackerVal!=null && versionTrackerVal.isEmpty()) { versionTrackerVal = null;}

        if( versionTrackerVal!=null)
        {
            this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.acversion", versionTrackerVal );
            if( !versionTrackerVal.isEmpty())
            {
                this.acUpdateService.check();
            }

        }
        else
        {
            this.pluginSettingsFactory.createGlobalSettings().remove("com.wittified.webfrags.acversion");

        }
		this.doGet( request, response);
	}


    private boolean checkForAC()
    {
        logger.debug("Checking for AC");
        return !System.getProperty("atlassian.upm.on.demand","").isEmpty();
    }
}
