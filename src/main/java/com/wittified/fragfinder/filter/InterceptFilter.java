package com.wittified.fragfinder.filter;

import com.wittified.fragfinder.service.DataTracker;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.*;
import java.net.Inet6Address;
import java.net.InetAddress;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class InterceptFilter implements Filter
{
    private final DataTracker dataTracker;

    public InterceptFilter( final DataTracker dataTracker)
    {
        this.dataTracker = dataTracker;
    }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException
    {
        filterChain.doFilter(servletRequest, servletResponse);

        if( servletRequest instanceof HttpServletRequest)
        {
            try
            {
                HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;

                Map<String, String> headers = new HashMap<String, String>();

                Enumeration httpHeaders = httpServletRequest.getHeaderNames();
                while(httpHeaders.hasMoreElements())
                {
                    String headerName = (String) httpHeaders.nextElement();
                    headers.put( headerName, httpServletRequest.getHeader( headerName));
                }
  //              String body =  httpServletRequest.getBody();


                String protocol = httpServletRequest.getScheme();
                String  host = httpServletRequest.getRemoteHost();

                if(host.equals("127.0.0.1")) { host = "localhost";}
                else if ( host.contains("0:0:0:0:0:0"))
                {
                    host = "localhost";
                }
                String path = httpServletRequest.getRequestURI();
                this.dataTracker.storeHTTPData( protocol, host,path, httpServletRequest.getMethod(), headers ,httpServletRequest.getQueryString(), "");

//                filterChain.doFilter(httpServletRequest, servletResponse);
            }
            catch(Exception e)
            {

            }
        }

    }

    @Override
    public void destroy() {

    }


}
