package com.wittified.fragfinder.conditions;


import com.atlassian.core.filters.ServletContextThreadLocal;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.servlets.PluginConfigurationServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 Copyright (c) 2013, Wittified, LLC
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the Wittified, LLC nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


public class JavascriptCondition implements Condition {
    private static final Logger logger = LoggerFactory.getLogger(JavascriptCondition.class);
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ApplicationProperties applicationProperties;

    public JavascriptCondition(final PluginSettingsFactory pluginSettingsFactory,
                               final ApplicationProperties applicationProperties) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> params)
    {
        if( this.isStash()) { return false;}
        logger.debug("Checking to see if we should display");
        Object settingVal = this.pluginSettingsFactory.createGlobalSettings().get(PluginConfigurationServlet.ALWAYS_DISPLAY_WEB_ITEM);
        if ((settingVal != null) && (Boolean.parseBoolean((String) settingVal))) {
            return true;
        }
        settingVal = this.pluginSettingsFactory.createGlobalSettings().get(PluginConfigurationServlet.ALWAYS_DISPLAY_WEB_PANEL);
        if ((settingVal != null) && (Boolean.parseBoolean((String) settingVal))) {
            return true;
        }

        HttpServletRequest httpServletRequest;

        logger.debug("Looking at request");
        // jira does request
        for(String key: params.keySet())
        {
            logger.debug( key );
        }
        if (params.containsKey("request")) {
            httpServletRequest = (HttpServletRequest) params.get("request");

            if ((httpServletRequest != null) && (httpServletRequest.getParameterMap() != null))
            {

                logger.debug("Looking at jira");
                if (httpServletRequest.getParameterMap().containsKey("web.items"))
                {
                    logger.debug("found web.items");
                    return true;
                }
                if (httpServletRequest.getParameterMap().containsKey("web.panels")) {
                    logger.debug("found web.panels");
                    return true;
                }
            }


        }

        // and Bamboo passes the query strings in...
        logger.debug("Looking at params");
        if (params.containsKey("web.items")) {
            logger.debug("found web.items");
            return true;
        }
        if (params.containsKey("web.panels")) {
            logger.debug("found web.panels");
            return true;
        }

        httpServletRequest = ServletContextThreadLocal.getRequest();
        logger.debug("Looking at the request context");

        if (httpServletRequest != null) {
            logger.debug( httpServletRequest.toString());
            if ((httpServletRequest != null) && (httpServletRequest.getParameterMap() != null))
            {
                if( httpServletRequest.getParameterMap().containsKey("web.items")) { logger.debug("Found web.items");return true;}
                if( httpServletRequest.getParameterMap().containsKey("web.panels")) {logger.debug("Found web.panels"); return true;}

            }
        }

        logger.debug("Not able to find anything");

        return false;
    }


    private boolean isStash()
    {
        return this.applicationProperties.getDisplayName().equalsIgnoreCase("stash");
    }

}