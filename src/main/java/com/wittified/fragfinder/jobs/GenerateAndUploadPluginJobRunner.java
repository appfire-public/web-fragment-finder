package com.wittified.fragfinder.jobs;

import com.atlassian.plugin.DefaultPluginArtifactFactory;
import com.atlassian.plugin.PluginArtifact;
import com.atlassian.plugin.PluginArtifactFactory;
import com.atlassian.plugin.PluginController;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.wittified.fragfinder.LocationEntry;
import com.wittified.fragfinder.LocationService;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by daniel on 3/8/15.
 */
public class GenerateAndUploadPluginJobRunner implements JobRunner
{
    private PluginController pluginController;
    private PluginArtifactFactory pluginArtifactFactory;
    private TemplateRenderer templateRenderer;
    private LocationService locationService;


    public GenerateAndUploadPluginJobRunner(final PluginController pluginController,
                                            final TemplateRenderer templateRenderer,
                                            final LocationService locationService)
    {
        this.pluginArtifactFactory = new DefaultPluginArtifactFactory();

        this.pluginController= pluginController;
        this.templateRenderer = templateRenderer;
        this.locationService = locationService;
    }

    @Override
    public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest)
    {
        this.uploadOurPlugin();
        return null;
    }




    public void uploadOurPlugin()
    {
        File item = this.generatePlugin();
        if ( item == null ) {
            return;
        }

        PluginArtifact pluginArtifact = this.pluginArtifactFactory.create(item.toURI());
        this.pluginController.installPlugins(pluginArtifact);

        item.delete();
    }


    private File generatePlugin()
    {
        try
        {
            File file = File.createTempFile("plugin-generator",".jar");

            FileOutputStream fos = new FileOutputStream(file);
            ZipOutputStream zipOut = new ZipOutputStream( fos );

            Map<String, String> resourceJS = this.generateWebResources();

            for (String item: resourceJS.keySet())
            {
                zipOut.putNextEntry( new ZipEntry( String.format( "web-resources/%s.js", item)) );
                zipOut.write( resourceJS.get( item).getBytes());
            }

            zipOut.putNextEntry(new ZipEntry("atlassian-plugin.xml"));
            zipOut.write(this.generatePluginXml().getBytes());


            zipOut.putNextEntry(new ZipEntry("com/wittified/fragfinder/conditions/WebItemCondition.class"));
            IOUtils.copy(this.getClass().getClassLoader().getResourceAsStream(
                    "com/wittified/fragfinder/conditions/WebItemCondition.class"), zipOut);


            zipOut.putNextEntry(new ZipEntry("com/wittified/fragfinder/conditions/WebPanelCondition.class"));
            IOUtils.copy(this.getClass().getClassLoader().getResourceAsStream(
                    "com/wittified/fragfinder/conditions/WebPanelCondition.class"), zipOut);

            zipOut.putNextEntry(new ZipEntry("com/wittified/fragfinder/conditions/WebSectionCondition.class"));
            IOUtils.copy(this.getClass().getClassLoader().getResourceAsStream(
                    "com/wittified/fragfinder/conditions/WebSectionCondition.class"), zipOut);

            zipOut.putNextEntry(new ZipEntry("com/wittified/fragfinder/contextProviders/CollectingWebItem.class"));
            IOUtils.copy(this.getClass().getClassLoader().getResourceAsStream(
                    "com/wittified/fragfinder/contextProviders/CollectingWebItem.class"), zipOut);





            zipOut.putNextEntry(new ZipEntry("com/wittified/fragfinder/conditions/WebResourceCondition.class"));
            IOUtils.copy(this.getClass().getClassLoader().getResourceAsStream(
                    "com/wittified/fragfinder/conditions/WebResourceCondition.class"), zipOut);


            zipOut.close();

            fos.close();
            fos = null;

            return file;
        }
        catch(SoyException e)
        {

        }
        catch(IOException e)
        {

        }
        return null;

    }


    private String generatePluginXml() throws SoyException, IOException
    {
        Map<String, Object> locationData = new HashMap<String, Object>();
        locationData.put("items", this.locationService.getWebItems());
        locationData.put("sections", this.locationService.getWebSections());
        locationData.put("panels", this.locationService.getWebPanels());
        locationData.put("resources", this.locationService.getWebResourceContexts());

        StringWriter stringWriter = new StringWriter();

        this.templateRenderer.render("templates/atlassian-plugin-xml.vm", locationData, stringWriter);
        return stringWriter.toString();

    }

    private Map<String, String> generateWebResources() throws IOException
    {
        Map<String, String> locationData = new HashMap<String, String>();

        for(LocationEntry locationEntry: this.locationService.getWebResourceContexts())
        {
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("webResource", locationEntry);
            data.put("action", this);
            StringWriter stringWriter = new StringWriter();
            this.templateRenderer.render("templates/web-resource-entry.vm", data, stringWriter);
            locationData.put( locationEntry.getId().replaceAll(":","-"), stringWriter.toString());



        }

        return locationData;
    }


    public String getWebResourceName( String location)
    {
        return "wittified-generated-web-resource-"+location.replaceAll(":","-");
    }

    public String getWebResourceJS( String location)
    {
        return location.replaceAll(":","-");
    }
}