package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.net.RequestFactory;
import com.dmurph.tracking.AnalyticsConfigData;
import com.dmurph.tracking.JGoogleAnalyticsTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by daniel on 11/17/14.
 */
public class UsageServiceImpl implements UsageService,LifecycleAware
{
    private final RequestFactory requestFactory;
    private final Logger logger = LoggerFactory.getLogger( UsageServiceImpl.class);

    public UsageServiceImpl( final RequestFactory requestFactory)
    {
        this.requestFactory = requestFactory;
    }

    @Override
    public void onStart()
    {
        this.ping("start");
    }

    @Override
    public void onStop()
    {

    }


    public void ping( String eventType)
    {
        if( System.getProperty("webfrag.allow.google.tracking", "false").equalsIgnoreCase("false") && (System.getenv("webfragsBlockGoogle")==null))
        {

            AnalyticsConfigData analyticsConfigData = new AnalyticsConfigData("UA-38030585-6");

            JGoogleAnalyticsTracker jGoogleAnalyticsTracker = new JGoogleAnalyticsTracker(analyticsConfigData, JGoogleAnalyticsTracker.GoogleAnalyticsVersion.V_4_7_2);

            jGoogleAnalyticsTracker.trackEvent("webFragment", eventType,"stash");
            logger.info("Sending usage statistics to Google. This can be disabled by starting the tomcat instance with -Dwebfrag.allow.google.tracking=true");
        }



    }


}