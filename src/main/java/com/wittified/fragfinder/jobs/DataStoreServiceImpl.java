package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.wittified.fragfinder.service.DataTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class DataStoreServiceImpl implements DataStoreService
{
    private final Logger logger = LoggerFactory.getLogger(DataStoreServiceImpl.class);
    private final DataTracker dataTracker;

    public DataStoreServiceImpl( final DataTracker dataTracker)
    {
        this.dataTracker = dataTracker;
    }

    public void store()
    {
        logger.debug("Transferring data");
        this.dataTracker.transferData();
    }

}
