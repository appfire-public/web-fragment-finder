package com.wittified.fragfinder.jobs;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.wittified.fragfinder.ao.CapturedEventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by daniel on 11/20/14.
 */
public class DeletionStoreJobRunner implements JobRunner {
    private static Logger logger = LoggerFactory.getLogger(DeletionStoreJobRunner.class);

    private final CapturedEventManager capturedEventManager;

    public DeletionStoreJobRunner(final CapturedEventManager capturedEventManager) {
        this.capturedEventManager = capturedEventManager;
    }

    @Override
    public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest) {
        this.capturedEventManager.expireContent();
        return null;
    }

}