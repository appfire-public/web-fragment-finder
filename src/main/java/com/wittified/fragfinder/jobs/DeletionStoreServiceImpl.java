package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.wittified.fragfinder.ao.CapturedEventManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 11/20/14.
 */
public class DeletionStoreServiceImpl implements DeletionStoreService
{
    private final Logger logger = LoggerFactory.getLogger(DeletionStoreServiceImpl.class);
    private final CapturedEventManager capturedEventManager;

    public DeletionStoreServiceImpl(final CapturedEventManager capturedEventManager)
    {
        this.capturedEventManager = capturedEventManager;
    }

    private boolean doNext = false;

    public void expire()
    {
        if(!doNext)

        {

            logger.debug("Transferring data");
            this.capturedEventManager.expireContent();
        }
        this.doNext = true;
    }

}
