package com.wittified.fragfinder.jobs;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.wittified.fragfinder.service.DataTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by daniel on 11/20/14.
 */
public class DataStoreJobRunner implements JobRunner
{
    private static Logger logger = LoggerFactory.getLogger(DataStoreJobRunner.class);

    private final DataTracker dataTracker;
    public DataStoreJobRunner(final DataTracker dataTracker)
    {
        this.dataTracker = dataTracker;
    }

    @Override
    public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest)
    {
        this.dataTracker.transferData();
        return null;
    }
}