package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import com.wittified.fragfinder.Constants;
import com.wittified.fragfinder.service.VersionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 12/5/14.
 */
public class ACUpdateServiceImpl implements ACUpdateService
{
    private final VersionService versionService;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ApplicationProperties applicationProperties;
    private final Logger logger = LoggerFactory.getLogger(DataStoreServiceImpl.class);

    public ACUpdateServiceImpl( final PluginSettingsFactory pluginSettingsFactory,
                                final VersionService versionService,
                                final ApplicationProperties applicationProperties)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.versionService = versionService;
        this.applicationProperties = applicationProperties;
    }



    public void check()
    {
        String val = (String) this.pluginSettingsFactory.createGlobalSettings().get("com.wittified.webfrags.acversion");
        if( val!=null && !val.isEmpty())
        {
            String version =  this.versionService.getCurrentPublishedVersion(val);
            if(version!=null) {
                if (!version.equals(this.applicationProperties.getVersion())) {
                    this.pluginSettingsFactory.createGlobalSettings().put("com.wittified.webfrags.ac.updateHost", version);
                } else {
                    this.pluginSettingsFactory.createGlobalSettings().remove("com.wittified.webfrags.ac.updateHost");
                }
            }
        }

        if( this.versionService.arePluginsUpToDate( val))
        {
            this.pluginSettingsFactory.createGlobalSettings().remove(Constants.SETTINGS_UPDATE_PLUGIN );
        }
        else
        {
            this.pluginSettingsFactory.createGlobalSettings().put(Constants.SETTINGS_UPDATE_PLUGIN, "true");
       }
    }

}
