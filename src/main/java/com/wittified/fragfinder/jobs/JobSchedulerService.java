package com.wittified.fragfinder.jobs;

import com.atlassian.plugin.PluginArtifactFactory;
import com.atlassian.plugin.PluginController;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.*;
import com.atlassian.templaterenderer.TemplateRenderer;
import com.wittified.fragfinder.LocationService;
import com.wittified.fragfinder.ao.CapturedEventManager;
import com.wittified.fragfinder.service.DataTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by daniel on 11/20/14.
 */
public class JobSchedulerService implements LifecycleAware
{
    private final SchedulerService schedulerService;
    private final DataTracker dataTracker;
    private final CapturedEventManager capturedEventManager;
    private final UsageService usageService;

    private PluginController pluginController;
    private TemplateRenderer templateRenderer;
    private LocationService locationService;
    private final ACUpdateService acUpdateService;


    private final Logger logger = LoggerFactory.getLogger(JobSchedulerService.class);

    private static final JobId DATA_STORE_JOB_ID = JobId.of("com.wittified.webfragment-finder-bitbucket:dataStoreJob");
    private static final long DATA_STORE_JOB_INTERVAL = TimeUnit.MINUTES.toMillis(5L);
    private static final String DATA_STORE_JOB_RUNNER_KEY = "com.wittified.webfragment-finder-bitbucket:dataStoreRunner";

    private static final JobId DELETION_STORE_JOB_ID = JobId.of("com.wittified.webfragment-finder-bitbucket:deletionStoreJob");
    private static final long DELETION_STORE_JOB_INTERVAL = TimeUnit.MINUTES.toMillis(5L);
    private static final String DELETION_STORE_JOB_RUNNER_KEY = "com.wittified.webfragment-finder-bitbucket:deletionStoreRunner";

    private static final JobId USAGE_STORE_JOB_ID = JobId.of("com.wittified.webfragment-finder-bitbucket:usageStoreJob");
    private static final long USAGE_STORE_JOB_INTERVAL = TimeUnit.HOURS.toMillis(1L);
    private static final String USAGE_STORE_JOB_RUNNER_KEY = "com.wittified.webfragment-finder-bitbucket:usageStoreRunner";



    private static final JobId UPLOAD_PLUGIN_JOB_ID = JobId.of("com.wittified.webfragment-finder-bitbucket:uploadPluginJob");
    private static final long UPLOAD_PLUGIN_JOB_INTERVAL = TimeUnit.HOURS.toMillis(1L);
    private static final String UPLOAD_PLUGIN_JOB_RUNNER_KEY = "com.wittified.webfragment-finder-bitbucket:uploadPluginRunner";





    private static final JobId AC_UPDATE_PLUGIN_JOB_ID = JobId.of("com.wittified.webfragment-finder-bitbucket:acUpdatePluginJob");
    private static final long AC_UPDATED_PLUGIN_JOB_INTERVAL = TimeUnit.HOURS.toMillis(1L);
    private static final String AC_UPDATE_PLUGIN_JOB_RUNNER_KEY = "com.wittified.webfragment-finder-bitbucket:acUpdatePluginRunner";




    public JobSchedulerService(final SchedulerService schedulerService,
                               final DataTracker dataTracker,
                               final CapturedEventManager capturedEventManager,
                               final UsageService usageService,
                               final PluginController pluginController,
                               final TemplateRenderer templateRenderer,
                               final LocationService locationService,
                               final ACUpdateService acUpdateService)
    {
        this.schedulerService = schedulerService;
        this.dataTracker = dataTracker;
        this.capturedEventManager = capturedEventManager;
        this.usageService = usageService;
        this.pluginController = pluginController;
        this.templateRenderer = templateRenderer;
        this.locationService = locationService;
        this.acUpdateService = acUpdateService;
    }

    @Override
    public void onStart()
    {

        System.err.println("Debugging");
        JobRunnerKey dataStoreKey = JobRunnerKey.of(DATA_STORE_JOB_RUNNER_KEY);
        DataStoreJobRunner dataStoreJobRunner = new DataStoreJobRunner(this.dataTracker);

        schedulerService.registerJobRunner( dataStoreKey, dataStoreJobRunner);

        try {
            schedulerService.scheduleJob(DATA_STORE_JOB_ID, JobConfig.forJobRunnerKey(dataStoreKey)
                    .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
                    .withSchedule(Schedule.forInterval(DATA_STORE_JOB_INTERVAL, new Date(System.currentTimeMillis()+600000))));
        }
        catch(SchedulerServiceException e)
        {
            e.printStackTrace();
        }




        JobRunnerKey deletionStoreJobKey = JobRunnerKey.of(DELETION_STORE_JOB_RUNNER_KEY);
        DeletionStoreJobRunner deletionStoreJobRunner = new DeletionStoreJobRunner(this.capturedEventManager);

        schedulerService.registerJobRunner( deletionStoreJobKey, deletionStoreJobRunner);

        try {
            schedulerService.scheduleJob(DELETION_STORE_JOB_ID, JobConfig.forJobRunnerKey(deletionStoreJobKey)
                    .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
                    .withSchedule(Schedule.forInterval(DELETION_STORE_JOB_INTERVAL, new Date(System.currentTimeMillis()+30000))));
        }
        catch(SchedulerServiceException e)
        {
            e.printStackTrace();
        }



        JobRunnerKey usageKey = JobRunnerKey.of(USAGE_STORE_JOB_RUNNER_KEY);
        UsageReportJobRunner usageReportJobRunner = new UsageReportJobRunner(this.usageService);

        schedulerService.registerJobRunner( usageKey, usageReportJobRunner);

        try {
            schedulerService.scheduleJob(USAGE_STORE_JOB_ID, JobConfig.forJobRunnerKey(usageKey)
                    .withRunMode(RunMode.RUN_ONCE_PER_CLUSTER)
                    .withSchedule(Schedule.forInterval(USAGE_STORE_JOB_INTERVAL, new Date(System.currentTimeMillis()))));
        }
        catch(SchedulerServiceException e)
        {
            e.printStackTrace();
        }



        JobRunnerKey uploadKey = JobRunnerKey.of(UPLOAD_PLUGIN_JOB_RUNNER_KEY);
        GenerateAndUploadPluginJobRunner generateAndUploadPluginJobRunner = new GenerateAndUploadPluginJobRunner(this.pluginController,
                this.templateRenderer, this.locationService);

        schedulerService.registerJobRunner( uploadKey, generateAndUploadPluginJobRunner);

        try {
            schedulerService.scheduleJob(UPLOAD_PLUGIN_JOB_ID, JobConfig.forJobRunnerKey(uploadKey)
                    .withRunMode(RunMode.RUN_LOCALLY)
                    .withSchedule(Schedule.forInterval(UPLOAD_PLUGIN_JOB_INTERVAL, new Date(System.currentTimeMillis()+60000))));
        }
        catch(SchedulerServiceException e)
        {
            e.printStackTrace();
        }


        JobRunnerKey acUpdateKey = JobRunnerKey.of(AC_UPDATE_PLUGIN_JOB_RUNNER_KEY);
        ACUpdateJobRunner acJobRunner = new ACUpdateJobRunner(this.acUpdateService);

        schedulerService.registerJobRunner( acUpdateKey, acJobRunner);

        try {
            schedulerService.scheduleJob(AC_UPDATE_PLUGIN_JOB_ID, JobConfig.forJobRunnerKey(acUpdateKey)
                    .withRunMode(RunMode.RUN_LOCALLY)
                    .withSchedule(Schedule.forInterval(AC_UPDATED_PLUGIN_JOB_INTERVAL, new Date(System.currentTimeMillis()+60000))));
        }
        catch(SchedulerServiceException e)
        {
            e.printStackTrace();
        }




    }

    @Override
    public void onStop()
    {
        this.schedulerService.unscheduleJob( DATA_STORE_JOB_ID);
        this.schedulerService.unscheduleJob( DELETION_STORE_JOB_ID);
        this.schedulerService.unscheduleJob( AC_UPDATE_PLUGIN_JOB_ID);
        this.schedulerService.unscheduleJob( USAGE_STORE_JOB_ID);
    }

}