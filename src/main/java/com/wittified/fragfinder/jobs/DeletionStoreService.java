package com.wittified.fragfinder.jobs;

/**
 * Created by daniel on 11/20/14.
 */
public interface DeletionStoreService
{

    public void expire();
}
