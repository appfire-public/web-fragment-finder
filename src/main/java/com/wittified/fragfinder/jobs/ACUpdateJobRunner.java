package com.wittified.fragfinder.jobs;

import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.wittified.fragfinder.Constants;
import com.wittified.fragfinder.service.VersionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * Created by daniel on 12/5/14.
 */
public class ACUpdateJobRunner implements JobRunner {
    private static Logger logger = LoggerFactory.getLogger(ACUpdateJobRunner.class);

    private final ACUpdateService acUpdateService;
    public ACUpdateJobRunner( final ACUpdateService acUpdateService )
    {
        this.acUpdateService = acUpdateService;
    }

    @Override
    public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest)
    {
        this.acUpdateService.check();
        return null;
    }
}


