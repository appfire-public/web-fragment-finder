package com.wittified.fragfinder.jobs;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Created by daniel on 11/17/14.
 */
public class UsageReportJobRunner implements JobRunner
{
    private static Logger logger = LoggerFactory.getLogger(UsageReportJobRunner.class);

    private final UsageService usageService;

    public UsageReportJobRunner(final UsageService usageService)
    {
        this.usageService = usageService;
    }

    @Override
    public JobRunnerResponse runJob(JobRunnerRequest jobRunnerRequest)
    {
        this.usageService.ping("active");

        return null;
    }
}