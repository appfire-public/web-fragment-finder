package com.wittified.fragfinder.contextProviders;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.sal.api.web.context.HttpContext;

import javax.servlet.http.HttpSession;
import java.util.*;

/**
 * Created by daniel on 2/6/15.
 */
public class WebItemDataProvider implements ContextProvider
{
    private final HttpContext httpContext;

    public WebItemDataProvider( final HttpContext httpContext)
    {
        this.httpContext = httpContext;
    }
    @Override
    public void init(Map<String, String> params) throws PluginParseException
    {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> params)
    {

        String entryName = (String) this.httpContext.getRequest().getAttribute("wittified.requestName");
        if(entryName==null)
        {
            entryName =  "webfrags-items:"+this.httpContext.getRequest().getRequestURI()+":"+System.currentTimeMillis();
            this.httpContext.getRequest().setAttribute("wittified.requestName", entryName);
        }

        params.put("requestName", entryName);


        // purge the session items
        HttpSession httpSession = this.httpContext.getSession(true);
        Enumeration keys = httpSession.getAttributeNames();

        Map< String, Date> sessionKeys = new WeakHashMap<String, Date>();
        List< Date> sessionDates = new ArrayList<Date>();
        while(keys.hasMoreElements())
        {
            String k = (String)keys.nextElement();
            if(k.startsWith("webfrags-items:") && !k.endsWith(":time"))
            {
                Date d = (Date) httpSession.getAttribute(k+":time");
                if(d!=null)
                {
                    sessionKeys.put(k, d);
                    sessionDates.add(d);
                }
            }
        }

        if(sessionDates.size()>5)
        {
            Collections.sort( sessionDates);
            Collections.reverse(sessionDates);
            Date cutOfDate = sessionKeys.get(5);
            if(cutOfDate!=null) {

                for (String sessionKey : sessionKeys.keySet()) {
                    Date newDate = sessionKeys.get(sessionKey);
                    if ((newDate != null) && (newDate.before(cutOfDate))) {
                        httpSession.removeAttribute(sessionKey);
                        httpSession.removeAttribute(sessionKey + ":time");
                    }
                }
            }

        }

        return params;
    }
}
