package com.wittified.fragfinder.contextProviders;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.webresource.WebResourceManager;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.wittified.fragfinder.Constants;

import java.util.Map;

/**
 * Created by daniel on 12/5/14.
 */
public class ACHostUpdateProvider implements ContextProvider {
    private final WebResourceManager webResourceManager;
    private final PluginSettingsFactory pluginSettingsFactory;
    private final ApplicationProperties applicationProperties;


    public ACHostUpdateProvider(final WebResourceManager webResourceManager,
                                final PluginSettingsFactory pluginSettingsFactory,
                                final ApplicationProperties applicationProperties
                                ) {
        this.webResourceManager = webResourceManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.applicationProperties = applicationProperties;

    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> params)
    {

        String version = (String) this.pluginSettingsFactory.createGlobalSettings().get("com.wittified.webfrags.ac.updateHost");

        params.put("current", this.applicationProperties.getVersion());
        if( version==null)
        {
            params.put("new","");
        }
        else
        {

            params.put("new", version);
        }
        params.put("shouldUpdateContents", this.pluginSettingsFactory.createGlobalSettings().get(Constants.SETTINGS_UPDATE_PLUGIN)!=null);

        this.webResourceManager.requireResourcesForContext("webfrags-ac-version-panel");
        return params;
    }
}