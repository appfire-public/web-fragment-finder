package com.wittified.fragfinder.contextProviders;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceManager;

import java.util.Map;

/**
 * Created by daniel on 11/1/14.
 */
public class JSContextProvider implements ContextProvider
{
    private final WebResourceManager webResourceManager;

    public JSContextProvider( final WebResourceManager webResourceManager)
    {
        this.webResourceManager = webResourceManager;
    }

    @Override
    public void init(Map<String, String> params) throws PluginParseException {

    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> params)
    {
        this.webResourceManager.requireResourcesForContext("webfrags-javascript-panel");
        return params;
    }
}