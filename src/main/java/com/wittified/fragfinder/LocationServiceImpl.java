package com.wittified.fragfinder;

/**
 Copyright (c) 2013, Wittified, LLC
 All rights reserved.

 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 * Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 * Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 * Neither the name of the Wittified, LLC nor the
 names of its contributors may be used to endorse or promote products
 derived from this software without specific prior written permission.

 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL Wittified, LLC BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebPanelModuleDescriptor;
import com.atlassian.plugin.web.descriptors.WebSectionModuleDescriptor;
import com.atlassian.plugin.webresource.WebResourceModuleDescriptor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LocationServiceImpl implements LocationService
{

	private final PluginAccessor pluginAccessor;

	public LocationServiceImpl(final PluginAccessor pluginManager)
	{
		this.pluginAccessor = pluginManager;
	}


	public List<LocationEntry> getWebItems()
	{
		HashMap<String, String> locations = new HashMap<String, String>();
		for (WebItemModuleDescriptor w : this.pluginAccessor.getEnabledModuleDescriptorsByClass( com.atlassian.plugin.web.descriptors.WebItemModuleDescriptor.class))
		{
			String section = w.getSection();
			String pluginKey = w.getPluginKey();

			locations.put( section, pluginKey);
		}
		return this.convertLocations( locations);

	}

	public List<LocationEntry> getWebSections()
	{
		HashMap<String, String> locations = new HashMap<String, String>();

		for (WebSectionModuleDescriptor w : this.pluginAccessor.getEnabledModuleDescriptorsByClass( WebSectionModuleDescriptor.class))
		{
			String section = w.getLocation();
			String pluginKey = w.getPluginKey();

			locations.put(section, pluginKey);
		}
		return this.convertLocations(locations);

	}

	public List<LocationEntry> getWebPanels()
	{
		HashMap<String, String> locations = new HashMap<String, String>();

		for (WebPanelModuleDescriptor w : this.pluginAccessor.getEnabledModuleDescriptorsByClass( WebPanelModuleDescriptor.class))
		{
			String section = w.getLocation();
			String pluginKey = w.getPluginKey();
			locations.put(section, pluginKey);
		}
		return this.convertLocations(locations);

	}


	public List<LocationEntry> getWebResourceContexts()
	{
		HashMap<String, String> locations = new HashMap<String, String>();

		for (WebResourceModuleDescriptor w : this.pluginAccessor.getEnabledModuleDescriptorsByClass( WebResourceModuleDescriptor.class))
		{
			for(String context: w.getContexts())
			{
				String pluginKey = w.getPluginKey();
				if(!pluginKey.contains("wittified"))
				{
					locations.put(context, pluginKey);
				}
			}
		}
		return this.convertLocations(locations);
	}

	private List<LocationEntry> convertLocations( Map<String, String> items)
	{
		List<LocationEntry> locationEntries = new ArrayList<LocationEntry>();
		for( String location: items.keySet())
		{
			if( location!=null)
			{
				String name = location.replaceAll(":","-");
				locationEntries.add( new LocationEntry(location, location, items.get( location)));
			}
		}
		return locationEntries;
	}


}
