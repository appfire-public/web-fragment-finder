package com.wittified.fragfinder;

/**
 * Created by daniel on 3/6/15.
 */
public class Constants
{
    public final static String PROXY_ENABLED_KEY = "wittified.webfrags.proxy.enabled";

    public final static String SETTINGS_UPDATE_PLUGIN = "com.wittified.webfrags.ac.updatePlugins";
}
