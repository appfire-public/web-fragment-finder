package com.wittified.fragfinder.ao;

import net.java.ao.Entity;
import net.java.ao.schema.StringLength;

/**
 * Created by daniel on 11/20/14.
 */
public interface CapturedHTTPHeader extends Entity
{
    public String getName();
    public void setName( String name);

    public Integer getType();
    public void setType( Integer type);

    @StringLength(StringLength.UNLIMITED)
    public String getValue();
    public void setValue( String value );

    public void setCapturedHTTP( CapturedHTTP capturedHTTP);
    public CapturedHTTP getCapturedHTTP();

}

