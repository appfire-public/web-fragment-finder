package com.wittified.fragfinder.ao;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/10/14.
 */
public interface CapturedEventManager
{
    public void registerEvent( final Date date, final String name, final  Map<String,String> params);

    public List<CapturedEvent> getEvents();
    public List<TrackerContainer> getEventNames();
    public PaginationEvents getEvents(int start, int end, String filter, final String order);

    public void resetEvents();

    public void registerHTTP( final Date date, final String protocol, final String server, final String path, String method, String query,  Map<String,String> headers, String body);
    public void registerOutHTTP( final Date date, final String protocol, final String server, final String path, String method, String query,  Map<String,String> headers, String body, Map<String,String> requestHeaders, String requestBody);

    public List<CapturedHTTP> getHTTP(boolean inEnabled, boolean outEnabled);
    public List<CapturedHTTP> getHTTP( String host, boolean inEnabled, boolean outEnabled);

    public PaginationHttp getHTTP(int start, int end,  final String order, boolean inEnabled, boolean outEnabled);
    public PaginationHttp getHTTP( int start, int end, final String order, String host, boolean inEnabled, boolean outEnabled);

    public int countHttpHosts( String filter,  boolean outEnabled, boolean inEnabled);

    public List<TrackerContainer> getHosts();
    public void resetHttp();

    public void expireContent();
}
