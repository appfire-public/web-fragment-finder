package com.wittified.fragfinder.ao;

import net.java.ao.Entity;
import net.java.ao.OneToMany;
import net.java.ao.schema.Indexed;
import net.java.ao.schema.StringLength;

import java.util.Date;

/**
 * Created by daniel on 11/20/14.
 */
public interface CapturedHTTP extends Entity
{
    @Indexed
    public Integer getDirection();
    public void setDirection( Integer direction);

    public Date getTime();
    public void setTime( Date date);

    public String getMethod();
    public void setMethod( String method);

    public String getProto();
    public void setProto( String protocol);

    public String getServer();
    public void setServer( String server);

    public String getPath();
    public void setPath( String path);

    public String getQueryStr();
    public void setQueryStr( String queryStr);

    @StringLength(StringLength.UNLIMITED)
    public String getBody();
    public void setBody( String body );

    @StringLength(StringLength.UNLIMITED)
    public String getReqBody();
    public void setReqBody( String body );

    @OneToMany
    public CapturedHTTPHeader[] getCapturedHTTPHeaders();
}
