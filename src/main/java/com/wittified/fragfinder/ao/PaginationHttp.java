package com.wittified.fragfinder.ao;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/20/14.
 */
@JsonSerialize
public class PaginationHttp
{
    @JsonSerialize
    Boolean previous;
    @JsonSerialize
    Boolean next;
    @JsonSerialize
    List<CapturedHTTP> https;

    public PaginationHttp() {}

    public Boolean getPrevious() {
        return previous;
    }

    public void setPrevious(Boolean previous) {
        this.previous = previous;
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public List<CapturedHTTP> getHttps() {
        return https;
    }

    public void setHttps(List<CapturedHTTP> https) {
        this.https = https;
    }
}
