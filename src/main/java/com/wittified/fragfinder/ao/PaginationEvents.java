package com.wittified.fragfinder.ao;

import com.wittified.fragfinder.rest.entities.CapturedEventEntity;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/20/14.
 */
@JsonSerialize
public class PaginationEvents
{
    @JsonSerialize
    Boolean previous;
    @JsonSerialize
    Boolean next;
    @JsonSerialize
    List<CapturedEvent> events;

    public PaginationEvents() {}

    public Boolean getPrevious() {
        return previous;
    }

    public void setPrevious(Boolean previous) {
        this.previous = previous;
    }

    public Boolean getNext() {
        return next;
    }

    public void setNext(Boolean next) {
        this.next = next;
    }

    public List<CapturedEvent> getEvents() {
        return events;
    }

    public void setEvents(List<CapturedEvent> events) {
        this.events = events;
    }
}
