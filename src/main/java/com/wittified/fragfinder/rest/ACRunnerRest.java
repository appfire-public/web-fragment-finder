package com.wittified.fragfinder.rest;

import com.atlassian.plugin.*;
import com.wittified.fragfinder.rest.entities.ACRunState;
import com.wittified.fragfinder.service.ACERunner;
import com.wittified.fragfinder.service.DescriptorService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 3/5/15.
 */
@Path("/acrun")
public class ACRunnerRest
{
    private final ACERunner aceRunner;

    public ACRunnerRest( final ACERunner aceRunner
                    )
  {

      this.aceRunner = aceRunner;
    }

    @GET
    @Path("/state")
    @Produces({MediaType.APPLICATION_JSON})
    public Response state()
    {
        return Response.ok(
                true).build();
    }

    @POST
    @Path("/start")
    @Produces({MediaType.APPLICATION_JSON})
    @Consumes({MediaType.APPLICATION_JSON})
    public Response start( ACRunState data)
    {
        this.aceRunner.start( new File( data.directory), data.command);


        return Response.ok().build();
    }


    @POST
    @Path("/stop")
    @Consumes({MediaType.APPLICATION_JSON})
    @Produces({MediaType.APPLICATION_JSON})
    public Response stop(Map<String, String> data)
    {
        this.aceRunner.stop();
        return Response.ok(true).build();
    }
}
