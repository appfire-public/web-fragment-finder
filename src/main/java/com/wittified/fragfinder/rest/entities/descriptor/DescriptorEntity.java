package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

/**
 * Created by daniel on 11/3/14.
 */

@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class DescriptorEntity
{
    public String key;
    public String name;
    public String version;
    public String description;
    public VendorEntity vendor;
    public LinksEntity  links;
    public LifeCycleEntity lifecycle;
    public String baseUrl;
    public Boolean enableLicensing;
    public AuthenticationEntity authentication;
    public ModuleEntities modules;
    public List<String> scopes;

    public DescriptorEntity() {}

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public VendorEntity getVendor() {
        return vendor;
    }

    public void setVendor(VendorEntity vendor) {
        this.vendor = vendor;
    }

    public LinksEntity getLinks() {
        return links;
    }

    public void setLinks(LinksEntity links) {
        this.links = links;
    }

    public LifeCycleEntity getLifecycle() {
        return lifecycle;
    }

    public void setLifecycle(LifeCycleEntity lifecycle) {
        this.lifecycle = lifecycle;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public Boolean getEnableLicensing() {
        return enableLicensing;
    }

    public void setEnableLicensing(Boolean enableLicensing) {
        this.enableLicensing = enableLicensing;
    }

    public AuthenticationEntity getAuthentication() {
        return authentication;
    }

    public void setAuthentication(AuthenticationEntity authentication) {
        this.authentication = authentication;
    }

    public ModuleEntities getModules() {
        return modules;
    }

    public void setModules(ModuleEntities modules) {
        this.modules = modules;
    }

    public List<String> getScopes() {
        return scopes;
    }

    public void setScopes(List<String> scopes) {
        this.scopes = scopes;
    }
}
