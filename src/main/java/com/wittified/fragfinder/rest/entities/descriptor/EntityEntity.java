package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Map;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class EntityEntity
{
    public String name;
    public String key;
    public String entityType;
    public Map<String, Object> keyConfigurations;

    public EntityEntity() {}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getEntityType() {
        return entityType;
    }

    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    public Map<String, Object> getKeyConfigurations() {
        return keyConfigurations;
    }

    public void setKeyConfigurations(Map<String, Object> keyConfigurations) {
        this.keyConfigurations = keyConfigurations;
    }
}
