package com.wittified.fragfinder.rest.entities.descriptor;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * Created by daniel on 11/3/14.
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class LayOutEntity
{
    public String height;

    public String width;

    public LayOutEntity() {}

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }
}
