package com.wittified.fragfinder.rest.entities;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/10/14.
 */
@JsonSerialize
public class CapturedHttpEntity
{
    public Integer direction;
    public String server;
    public String path;
    public String protocol;
    public String method;
    public String queryString;
    public List<Map<String, String>> requestHeaders;
    public List<Map<String, String>> responseHeaders;
    public Date time;
    public String responseBody;
    public String requestBody;

    public CapturedHttpEntity() {}

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public List<Map<String, String>> getResponseHeaders() {
        return responseHeaders;
    }

    public void setResponseHeaders(List<Map<String, String>> headers) {
        this.responseHeaders = headers;
    }


    public List<Map<String, String>> getRequestHeaders() {
        return requestHeaders;
    }

    public void setRequestHeaders(List<Map<String, String>> headers) {
        this.requestHeaders = headers;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getRequestBody() {
        return requestBody;
    }

    public void setRequestBody(String body) {
        this.requestBody = body;
    }
    public String getResponseBody() {
        return requestBody;
    }

    public void setResponseBody(String body) {
        this.requestBody = body;
    }
}
