package com.wittified.fragfinder.rest;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.user.UserManager;
import com.wittified.fragfinder.servlets.PluginConfigurationServlet;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by daniel on 10/31/14.
 */
@Path("/config")
public class CopyPasteEndPoint
{
    private final PluginSettingsFactory pluginSettingsFactory;
    private final UserManager userManager;

    public CopyPasteEndPoint( final PluginSettingsFactory pluginSettingsFactory,
                              final UserManager userManager)
    {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.userManager =userManager;
    }

    @GET
    @Produces({MediaType.APPLICATION_JSON})
    @AnonymousAllowed
    @Path("/")
    public Response getCurrentSetting()
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }


        Map<String, String> map = new HashMap<String, String>();
        map.put("value",  PluginConfigurationServlet.COPY_PASTE_FORMAT_AC);

        Object item = this.pluginSettingsFactory.createGlobalSettings().get( PluginConfigurationServlet.COPY_PASTE_FORMAT_KEY );
        if(item!=null)
        {
            map.put("value", (String) item);
        }
        return Response.ok( map).build();
    }
}
