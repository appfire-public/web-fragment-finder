package com.wittified.fragfinder.rest;

import com.atlassian.sal.api.user.UserManager;
import com.wittified.fragfinder.ao.*;
import com.wittified.fragfinder.rest.entities.CapturedHttpEntity;
import com.wittified.fragfinder.rest.entities.HttpRequest;
import com.wittified.fragfinder.rest.entities.NavEntryEntity;
import com.wittified.fragfinder.rest.entities.PaginationHTTPEntity;
import com.wittified.fragfinder.service.DataTracker;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 11/10/14.
 */
@Path("/webfrags/http")
public class HttpDataResource
{
    private final CapturedEventManager capturedEventManager;
    private final UserManager userManager;
    private final DataTracker dataTracker;

    public HttpDataResource(final CapturedEventManager capturedEventManager,
                            final UserManager userManager,
                            final DataTracker dataTracker)
    {
        this.capturedEventManager = capturedEventManager;
        this.userManager = userManager;
        this.dataTracker = dataTracker;
    }


    @GET
    @Path("/numItems")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getNumberOfItems( @QueryParam("filter") String filter, @QueryParam("out") Boolean outEnabled, @QueryParam("in") Boolean inEnabled)
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }
        return Response.ok(
            this.capturedEventManager.countHttpHosts( filter, outEnabled, inEnabled)
        ).build();
    }


    @GET
    @Path("/page/{pageNum}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPaginatedEvents( @PathParam("pageNum") Integer pageNum, @QueryParam("sort") String sortBy, @QueryParam("numItems") Integer numItems, @QueryParam("filter") String filter, @QueryParam("out") Boolean outEnabled, @QueryParam("in") Boolean inEnabled )
    {
        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }
        pageNum--;
        if( numItems==null)
        {
            numItems = 20;
        }
        if( filter == null)
        {
            filter = "";
        }

        PaginationHTTPEntity paginationHTTPEntity = new PaginationHTTPEntity();

        PaginationHttp paginationHttp = null;

        if( filter==null || filter.isEmpty())
        {
            paginationHttp =  this.capturedEventManager.getHTTP(pageNum * numItems, (pageNum + 1) * numItems, "TIME", inEnabled, outEnabled);
        }
        else
        {
            paginationHttp =  this.capturedEventManager.getHTTP(pageNum*numItems,(pageNum+1)* numItems, filter, "TIME", inEnabled, outEnabled);

        }

        paginationHTTPEntity.setNext( paginationHttp.getNext());
        paginationHTTPEntity.setPrevious( paginationHttp.getPrevious());


        List<CapturedHttpEntity> capturedHttpRequests = new ArrayList<CapturedHttpEntity>();
        if( paginationHttp!=null && !paginationHttp.getHttps().isEmpty()) {
            for (CapturedHTTP capturedHTTP : paginationHttp.getHttps()) {
                capturedHttpRequests.add(this.convert(capturedHTTP));
            }
        }
        paginationHTTPEntity.setHttpRequests( capturedHttpRequests);

        return Response.ok( paginationHTTPEntity ).build();

    }


    @GET
    @Path("/all")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllEvents(@QueryParam("filter") String filter, @QueryParam("out") Boolean outEnabled, @QueryParam("in") Boolean inEnabled)
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        List<CapturedHTTP> capturedHTTPs = null;
        if( filter==null || filter.isEmpty())
        {
            capturedHTTPs = this.capturedEventManager.getHTTP(inEnabled, outEnabled);
        }
        else
        {
            capturedHTTPs = this.capturedEventManager.getHTTP( filter,inEnabled, outEnabled);
        }
        List<CapturedHttpEntity> capturedHttpEntities = new ArrayList<CapturedHttpEntity>();
        for( CapturedHTTP capturedHTTP : capturedHTTPs)
        {
            capturedHttpEntities.add( this.convert(capturedHTTP));
        }

        return Response.ok( capturedHttpEntities).build();

    }



    @GET
    @Path("/serverNames")
    @Produces( {MediaType.APPLICATION_JSON})
    public Response getEventNames()
    {


        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        this.dataTracker.transferData();

        List<NavEntryEntity> navEntryEntities = new ArrayList<NavEntryEntity>();
        for( TrackerContainer nav: this.capturedEventManager.getHosts())
        {
            NavEntryEntity navEntryEntity = new NavEntryEntity();
            navEntryEntity.setKey( nav.getNameTracker().getKey());
            navEntryEntity.setName( nav.getNameTracker().getLabel());
            navEntryEntity.setNumItems( nav.getNumItems());
            navEntryEntities.add( navEntryEntity);
        }
        return Response.ok( navEntryEntities).build();

    }



    @POST
    @Path("/refresh")
    @Produces({MediaType.APPLICATION_JSON})
    public Response refresh()
    {


        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        this.dataTracker.transferData();
        return Response.ok().build();
    }

    @GET
    @Path("/reset")
    @Produces({MediaType.APPLICATION_JSON})
    public Response reset()
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }
        this.capturedEventManager.resetHttp();
        return Response.ok().build();
    }

    protected CapturedHttpEntity convert(CapturedHTTP capturedHTTP)
    {
        CapturedHttpEntity capturedHttpEntity = new CapturedHttpEntity();
        capturedHttpEntity.setPath(capturedHTTP.getPath());
        capturedHttpEntity.setServer(capturedHTTP.getServer());
        capturedHttpEntity.setProtocol(capturedHTTP.getProto());
        capturedHttpEntity.setDirection( capturedHTTP.getDirection());
        capturedHttpEntity.setTime( capturedHTTP.getTime());
        capturedHttpEntity.setMethod( capturedHTTP.getMethod());
        capturedHttpEntity.setQueryString( capturedHTTP.getQueryStr());
        capturedHttpEntity.setResponseBody( capturedHTTP.getBody());
        capturedHttpEntity.setRequestBody( capturedHTTP.getReqBody());

        if( capturedHttpEntity.getQueryString()==null){ capturedHttpEntity.setQueryString("");}
        List<Map<String, String>> requestHeaders = new ArrayList<Map<String, String>>();
        List<Map<String, String>> responseHeaders = new ArrayList<Map<String, String>>();
        for(CapturedHTTPHeader p: capturedHTTP.getCapturedHTTPHeaders())
        {
            Map<String, String> entry = new HashMap<String, String>();
            entry.put("name", p.getName());
            entry.put("value", p.getValue());

            if(( p.getType()==null) || (p.getType()==0))
            {
                responseHeaders.add( entry);
            }
            else
            {
                requestHeaders.add( entry);
            }

        }

        capturedHttpEntity.setRequestHeaders( requestHeaders);
        capturedHttpEntity.setResponseHeaders( responseHeaders);

        return capturedHttpEntity;

    }
}
