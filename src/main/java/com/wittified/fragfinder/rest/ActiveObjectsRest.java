package com.wittified.fragfinder.rest;


import com.atlassian.activeobjects.external.ActiveObjects;
import com.atlassian.activeobjects.spi.DataSourceProvider;
import com.atlassian.activeobjects.spi.TenantAwareDataSourceProvider;
import com.atlassian.jira.tenancy.JiraTenantContext;
import com.atlassian.plugin.*;
import com.atlassian.plugin.module.ContainerManagedPlugin;
import com.atlassian.plugin.osgi.factory.OsgiPlugin;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.tenancy.api.TenantAccessor;
import com.atlassian.tenancy.api.helper.TenantAccessors;
import com.wittified.fragfinder.rest.entities.AOCellEntity;
import com.wittified.fragfinder.rest.entities.AORowEntity;
import com.wittified.fragfinder.rest.entities.PaginationAOEntity;
import net.java.ao.schema.Table;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.dom4j.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.security.MessageDigest;
import java.sql.*;
import java.util.*;
import java.util.regex.Pattern;

/**
 * Created by daniel on 11/21/14.
 */
@Path("/activeobjects")
public class ActiveObjectsRest
{
    private final PluginAccessor pluginAccessor;
    private final UserManager userManager;
    private final TenantAwareDataSourceProvider tenantAwareDataSourceProvider;
    private final JiraTenantContext jiraTenantContext;




    private static final Logger logger = LoggerFactory.getLogger( ActiveObjectsRest.class);

    public ActiveObjectsRest( final PluginAccessor pluginAccessor,
                              final UserManager userManager,
                              final TenantAwareDataSourceProvider tenantAwareDataSourceProvider,
                              final JiraTenantContext jiraTenantContext)
    {
        this.pluginAccessor = pluginAccessor;
        this.userManager = userManager;
        this.tenantAwareDataSourceProvider = tenantAwareDataSourceProvider;
        this.jiraTenantContext = jiraTenantContext;

    }

    @GET
    @Path("/plugins")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getPlugins()
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }


        List<Map<String, String>> entries = new ArrayList<Map<String, String>>();
        for( Plugin plugin: this.pluginAccessor.getEnabledPlugins()) {

            for (ModuleDescriptor moduleDescriptor : plugin.getModuleDescriptors())
            {
                try
                {

                    if (moduleDescriptor.getModule() instanceof ActiveObjects) {

                        Map<String, String> genericEntity = new HashMap<String, String>();
                        genericEntity.put("key", plugin.getKey());
                        genericEntity.put("name", plugin.getName());
                        entries.add(genericEntity);
                    }

                }catch(Exception e)
                {
                }

            }
        }

        return Response.ok(entries).build();
    }

    @GET
    @Path("/tables/{pluginKey}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getAllTables(@PathParam("pluginKey") String pluginKey)
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }


        List<String> tables = new ArrayList<String>();
        Plugin plugin = this.pluginAccessor.getPlugin( pluginKey);


        try {



            Element aoNode = new SAXBuilder().build( ( (ContainerManagedPlugin) plugin).getPluginArtifact().getResourceAsStream("atlassian-plugin.xml")).getRootElement().getChild("ao");
            if( aoNode!=null)
            {
                for(Element entity: aoNode.getChildren("entity"))
                {
                    tables.add(entity.getTextTrim());
                }

            }



        }
        catch(Exception e)
        {
            e.printStackTrace();
        }

        return Response.ok(tables).build();
    }


    @GET
    @Path("/table/{pluginKey}/{table}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getMetaDataForTable(@PathParam("pluginKey") String pluginKey, @PathParam("table") String table)
    {

        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }


        List<Map<String, String>> entries = new ArrayList<Map<String, String>>();
        Plugin plugin = this.pluginAccessor.getPlugin( pluginKey);
        try
        {
            Class tableClass = plugin.getClassLoader().loadClass(table);
            for( Method method: tableClass.getMethods())
            {
                if( method.getName().startsWith("get"))
                {
                    String name = method.getName().substring(3);
                    if(!((name.equalsIgnoreCase("EntityType")) || ( name.equalsIgnoreCase("EntityManager"))))
                    {
                        String type = method.getReturnType().getCanonicalName();
                        Map<String, String> entity = new WeakHashMap<String, String>();
                        entity.put("name", name);
                        entity.put("type", type);
                        entries.add(entity);
                    }
                }
            }

        }
        catch(ClassNotFoundException classNotFoundException)
        {

        }

        return Response.ok(entries).build();
    }


    @GET
    @Path("/data/{pluginKey}/{table}/{pageNum}")
    @Produces({MediaType.APPLICATION_JSON})
    public Response getTableData(@PathParam("pluginKey") String pluginKey, @PathParam("table") String table,
                                 @PathParam("pageNum") Integer pageNum, @QueryParam("sort") String sortBy,
                                 @QueryParam("numItems") Integer numItems, @QueryParam("filter") String filter)
    {


        if( (this.userManager.getRemoteUsername()==null) || (!this.userManager.isAdmin( this.userManager.getRemoteUsername())) )
        {
            return Response.status(401).build();
        }

        pageNum--;
        if( numItems==null)
        {
            numItems = 20;
        }
        if( filter == null)
        {
            filter = "";
        }


        PaginationAOEntity page = new PaginationAOEntity();
        if( pageNum>0)
        {
            page.setPrevious(true);
        }
        int startAt = pageNum*numItems;

        String order = null;
        if( sortBy==null || sortBy.isEmpty())
        {
            order = " ID ";
        }
        else
        {
            order = sortBy.toUpperCase();
        }

        logger.debug("Got name as " + table);


        String tableName = this.extractTableName( table, pluginKey);
        if(tableName==null){
            return Response.ok().build();
        }
        tableName = tableName.replaceAll("[^\\w_]", "");



        List<Map<String, String>> headers = new ArrayList<Map<String, String>>();



        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try
        {


            conn =this.tenantAwareDataSourceProvider.getDataSource( this.jiraTenantContext.getCurrentTenant()).getConnection();
            statement = conn.createStatement( ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);

            logger.debug("Executing:  SELECT * FROM  " + tableName.toUpperCase());
            resultSet = statement.executeQuery(" SELECT * FROM  \"" + tableName.toUpperCase()+"\"");
            resultSet.setFetchSize(numItems+1);
            resultSet.absolute(startAt);

            logger.debug("Fetch size: " + numItems);
            logger.debug("Start: " + startAt);


            List<AORowEntity> rowData = new ArrayList<AORowEntity>();




            ResultSetMetaData resultSetMetaData = null;

            SQLWarning sqlWarning = resultSet.getWarnings();
            while(sqlWarning!=null)
            {
                logger.error(sqlWarning.getMessage());

                sqlWarning = sqlWarning.getNextWarning();
            }





            logger.debug("Looking for " + tableName);

            resultSetMetaData = resultSet.getMetaData();
            int numCols = resultSetMetaData.getColumnCount();

            for(int col=1;col<=numCols;col++)
            {
                Map<String, String> d = new WeakHashMap<String, String>();

                d.put("name", resultSetMetaData.getColumnName( col) );
                d.put("type", resultSetMetaData.getColumnTypeName(col) );
                headers.add(d);
            }



            while (resultSet.next())
            {
                AORowEntity aoRowEntity = new AORowEntity();
                List<AOCellEntity> aoCellEntities = new ArrayList<AOCellEntity>();

                logger.debug("Fetching a row");
                for(int col=1;col<=numCols;col++)
                {

                    try {

                        logger.debug(resultSetMetaData.getColumnName(col));
                        logger.debug(resultSet.getString(col));
                        AOCellEntity aoCellEntity = new AOCellEntity();
                        aoCellEntity.setName(resultSetMetaData.getColumnName(col));

                        String value = "";
                        switch (resultSetMetaData.getColumnType( col))
                        {
                            case Types.ARRAY:
                                break;
                            case Types.BIGINT:
                                value = Integer.toString( resultSet.getInt(col));
                                break;
                            case Types.BINARY:
                                value = IOUtils.toString( resultSet.getBinaryStream(col));
                                break;
                            case Types.BIT:
                                value = resultSet.getString(col);
                                break;
                            case Types.BLOB:
                                value = IOUtils.toString( resultSet.getBlob(col).getBinaryStream());
                                break;
                            case Types.BOOLEAN:
                                break;
                            case Types.CHAR:
                                value = resultSet.getString(col);
                                break;
                            case Types.CLOB:
                                value = IOUtils.toString( resultSet.getClob(col).getAsciiStream());
                                break;
                            case Types.DATALINK:
                                break;
                            case Types.DATE:
                                value = resultSet.getDate( col).toString();
                                break;
                            case Types.DECIMAL:
                                value = Float.toString( resultSet.getFloat( col));
                                break;
                            case Types.DISTINCT:
                                break;
                            case Types.DOUBLE:
                                value = Double.toString( resultSet.getDouble( col));
                                break;
                            case Types.FLOAT:
                                value = Float.toString( resultSet.getFloat( col));
                                break;
                            case Types.INTEGER:
                                value = Integer.toString( resultSet.getInt(col));
                                break;
                            case Types.JAVA_OBJECT:
                                value = resultSet.getObject(col).toString();
                                break;
                            case Types.LONGNVARCHAR:
                                value = resultSet.getString(col);
                                break;
                            case Types.LONGVARBINARY:
                                value = IOUtils.toString( resultSet.getBinaryStream(col));
                                break;
                            case Types.LONGVARCHAR:
                                value = resultSet.getString(col);
                                break;
                            case Types.NCHAR:
                                value = resultSet.getString(col);
                                break;
                            case Types.NCLOB:
                                value = IOUtils.toString( resultSet.getClob(col).getAsciiStream());
                                break;
                            case Types.NULL:
                                value = null;
                                break;
                            case Types.NUMERIC:
                                break;
                            case Types.NVARCHAR:
                                value = resultSet.getString(col);
                                break;
                            case Types.OTHER:
                                value = "";
                                break;
                            case Types.REAL:
                                value = Float.toString( resultSet.getFloat(col) );
                                break;
                            case Types.REF:
                                value = resultSet.getRef(col).toString();
                                break;
                            case Types.SMALLINT:
                                value = Integer.toString( resultSet.getInt(col));
                                break;
                            case Types.SQLXML:
                                value = resultSet.getSQLXML(col).toString();
                                break;
                            case Types.TIME:
                                value = resultSet.getTime( col).toString();
                                break;
                            case Types.TIMESTAMP:
                                value = resultSet.getTimestamp( col).toString();
                                break;
                            case Types.TINYINT:
                                value = Integer.toString( resultSet.getInt(col));
                                break;
                            case Types.VARBINARY:
                                value = IOUtils.toString( resultSet.getBinaryStream(col));
                                break;
                            case Types.VARCHAR:
                                value = resultSet.getString(col);
                                break;
                            default:
                                value = "Unknown";
                                break;
                        }
                        aoCellEntity.setValue(value);
                        aoCellEntities.add(aoCellEntity);
                    }
                    catch(Exception e)
                    {
                        logger.error(e.getMessage());
                        e.printStackTrace();
                    }
                }
                aoRowEntity.setEntries(aoCellEntities);
                rowData.add(aoRowEntity);
                logger.debug("Done");
            }

            logger.debug("Setting row");


            if( rowData.size()>numItems)
            {
                page.setNext(true);
                page.setRows(rowData.subList(0, numItems));
            }
            else
            {
                page.setNext(false);
                page.setRows( rowData);
            }


        }catch(SQLException sqlException)
        {
            sqlException.printStackTrace();
        }
        finally {
            if( resultSet !=null)
            {
                try
                {
                    resultSet.close();

                }catch(SQLException e)
                {
                    e.printStackTrace();
                }
                resultSet = null;
            } if( statement !=null)
            {
                try
                {
                    statement.close();

                }catch(SQLException e)
                {
                    e.printStackTrace();
                }
                statement = null;
            }
        }
        conn = null;



        if( startAt==0)
        {
            page.setPrevious(false);
        }
        else
        {
            page.setPrevious( true);
        }


        page.setHeaders( headers);
        return Response.ok( page  ).build();
    }

    private String extractTableName(  String table, String pluginKey)
    {


        try
        {

            Plugin plugin = this.pluginAccessor.getPlugin(pluginKey);
            String symbolic =pluginKey;

            if( plugin instanceof OsgiPlugin) {
                symbolic = ((OsgiPlugin) plugin).getBundle().getSymbolicName();

            }

            else
            {
                logger.debug("Doesn't look like an osgi plugin...");
                InputStream inputStream = null;
                try {
                    inputStream = plugin.getClassLoader().getResourceAsStream("META-INF/MANIFEST.MF");
                    if (inputStream != null) {
                        logger.debug("Found manifest");
                        Properties properties = new Properties();
                        properties.load(inputStream);
                        if (properties.containsKey("Bundle-SymbolicName")) {
                            logger.debug("Reading property");
                            String item = (String) properties.get("Bundle-SymbolicName");
                            if (item != null && !item.isEmpty()) {
                                logger.debug("Found: "+item);
                                symbolic = item;
                            }
                        }
                        inputStream.close();
                        inputStream = null;
                    }
                }catch(Exception e)
                {
                    e.printStackTrace();
                }
                finally
                {
                    if( inputStream!=null)
                    {
                        inputStream.close();
                    }
                }
            }


            Class tableClass = plugin.getClassLoader().loadClass(table);


            String prefix = symbolic;

            // check the atlassian-plugin.xml file


            try {

                System.err.println( plugin.getClass() );



                InputStream inputStream = plugin.getClassLoader().getResourceAsStream("atlassian-plugin.xml");


                if (inputStream != null) {


                    Element aoNode = new SAXBuilder().build(   ( (ContainerManagedPlugin) plugin).getPluginArtifact().getResourceAsStream("atlassian-plugin.xml")).getRootElement().getChild("ao");
                    if( aoNode!=null)
                    {

                        if( aoNode.getAttribute("namespace")!=null)
                        {
                            String namespace = aoNode.getAttribute("namespace").getValue();

                            if (!namespace.equals(""))
                            {
                                logger.debug("Using namespace: " + namespace);
                                prefix = namespace;
                            }

                        }

                    }

                    inputStream.close();
                    inputStream = null;
                }


            }
            catch(Exception e)
            {
                e.printStackTrace();
            }




            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(prefix.getBytes());


            byte byteData[] = md.digest();

            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < byteData.length; i++) {
                sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
            }

            String fullHexHash  = sb.toString();

            String tableName = "AO_"+fullHexHash.substring(fullHexHash.length()-6);

            String entryName = null;
            boolean isAnnotated = false;
            if(tableClass.isAnnotationPresent(Table.class))
            {

                logger.debug("Found annotation");
                entryName = ((Table)tableClass.getAnnotation(Table.class)).value();
            }
            logger.debug("Name: " + entryName);


            if( entryName==null)
            {
                logger.debug("getCanonicalName: " + tableClass.getCanonicalName());
                String[] items =tableClass.getCanonicalName().split("\\.");
                entryName =  items[items.length-1];

            }
            logger.debug("It was "+ entryName);
            entryName = Pattern.compile("([a-z\\d])([A-Z])").matcher(entryName ).replaceAll("$1_$2");
            logger.debug("Now is "+ entryName);

            return (tableName+"_"+ entryName).toUpperCase();


        }catch(Exception e)
        {
            e.printStackTrace();

        }
        return null;

    }

}