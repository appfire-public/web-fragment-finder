if(!window.wittified)
{
    window.wittified = {};
}
if(!wittified.webfragsDescriptor)
{
    window.wittified.webfragsDescriptor = {
        'selectActionDialog': false,
        'currentActionKey': false,
        'selectAction': function( key)
        {
            this.currentActionKey = key;
            if( !this.selectActionDialog)
            {
                this.selectActionDialog  = new AJS.Dialog({
                    width: 400,
                    height: 200,
                    id: 'select-action-dialog',
                    closeOnOutsideClick: true
                });

                this.selectActionDialog.addHeader('Do you want to edit this?');
                this.selectActionDialog.addPanel('Edit-panel', '<div id="wittified-selected-panel-action">'+com.wittified.fragfinder.client.editPreviousItem({})+'</div>');
                this.selectActionDialog.addButtonPanel();
                this.selectActionDialog.addButton('Go to', function(dialog)
                {


                    var item = AJS.$('#'+ wittified.webfragsDescriptor.currentActionKey);
                    item.data('letthrough','1');
                    item.click();
                    dialog.hide();

                });
                this.selectActionDialog.addButton('Edit', function(dialog)
                {
                    AJS.$.get( AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/descriptor/get/'+ wittified.webfragsDescriptor.currentActionKey, function(data)
                    {
                        wittified.webfragsDescriptor.addDescriptor('edit-'+data.type, data.module.location, data);
                    });
                    dialog.hide();
                });

            }
            this.selectActionDialog.show();
        },
        'addDescriptorDialog': false,
        'currentLocation':'',
        'addDescriptor': function(type, loc, data)
        {
           if(!data) { data = {module:{},type:'new'}};
            console.log('here');
             window.wittified.webfragsDescriptor.currentLocation = loc;
            if( !this.addDescriptorDialog)
            {
                this.addDescriptorDialog  = new AJS.Dialog({
                     width: 800,
                     height: 670,
                     id: 'descriptor-'+type+'--dialog',
                     closeOnOutsideClick: true
                 });

                 this.addDescriptorDialog.addHeader('Add Link');
                 this.addDescriptorDialog.addPanel('Link Type', '<div id="wittified-selected-type"></div>');




                 this.addDescriptorDialog.addButtonPanel();
                 this.addDescriptorDialog.addButton('Close', function(dialog)
                 {
                        dialog.hide();
                 });

                 this.addDescriptorDialog.addPage();


                 this.addDescriptorDialog.addHeader('Add Web-Item');
                 var time = new Date().getTime();
                 this.addDescriptorDialog.addPanel('Link info', com.wittified.fragfinder.client.decorator.renderWebItemUI({time:time,location:loc,data:data.module}));
                 this.addDescriptorDialog.addButtonPanel();
                 this.addDescriptorDialog.addButton('Save', function(dialog)
                 {
                    if( window.wittified.webfragsDescriptor.saveWebItem())
                    {
                        dialog.hide();
                    }

                 });


                  this.addDescriptorDialog.addPage();

                  this.addDescriptorDialog.addHeader('Add Admin Page');
                  var time = new Date().getTime();
                  this.addDescriptorDialog.addPanel('Link info', com.wittified.fragfinder.client.decorator.renderAdminPageUI({time:time,location:loc,data:data.module}));
                  this.addDescriptorDialog.addButtonPanel();
                  this.addDescriptorDialog.addButton('Save', function(dialog)
                  {
                     if( window.wittified.webfragsDescriptor.saveAdminPage())
                     {
                         dialog.hide();
                     }

                  });


                 this.addDescriptorDialog.addPage();

                 this.addDescriptorDialog.addHeader('Add General Page');
                 var time = new Date().getTime();
                 this.addDescriptorDialog.addPanel('Link info', com.wittified.fragfinder.client.decorator.renderGeneralPageUI({time:time,location:loc,data:data.module}));
                 this.addDescriptorDialog.addButtonPanel();
                 this.addDescriptorDialog.addButton('Save', function(dialog)
                 {
                    if( window.wittified.webfragsDescriptor.saveGeneralPage())
                    {
                        dialog.hide();
                    }

                 });


                 this.addDescriptorDialog.addPage();

                 this.addDescriptorDialog.addHeader('Add Web Panel');
                 var time = new Date().getTime();
                 this.addDescriptorDialog.addPanel('panel info', com.wittified.fragfinder.client.decorator.renderWebPanelUI({time:time,location:loc,data:data.module}));
                 this.addDescriptorDialog.addButtonPanel();
                 this.addDescriptorDialog.addButton('Save', function(dialog)
                 {
                    if( window.wittified.webfragsDescriptor.saveWebPanel())
                    {
                        dialog.hide();
                    }

                 });



            }

            AJS.$('#wittified-webfrags-target').change(function()
            {
                AJS.$('#wittified-webfrags-inlinedialog').hide();
                AJS.$('#wittified-webfrags-dialog').hide();

                AJS.$('#wittified-webfrags-'+AJS.$(this).val()).show();
            });
            AJS.$('#wittified-webfrags-inlinedialog').hide();
            AJS.$('#wittified-webfrags-dialog').hide();

            this.addDescriptorDialog.gotoPage(0);
            AJS.$('#wittified-selected-type').html(com.wittified.fragfinder.client.decorator.renderSelectType({'isAdminArea':loc.indexOf('admin')>-1}));

            AJS.$('#wittified-web-item-select').on('click', function()
            {
                window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(1);
            });

            AJS.$('#wittified-admin-select').on('click', function()
            {
                window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(2);
            });

            AJS.$('#wittified-general-select').on('click', function()
            {
                window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(3);
            });

            if( type=='web-panel')
            {
                window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(4);
            }
            if(data.type)
            {
                if(data.type=='webItem')
                {
                    window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(1);
                }

                else if(data.type=='adminPage')
                {
                    window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(2);
                }

                else if(data.type=='generalPage')
                {
                    window.wittified.webfragsDescriptor.addDescriptorDialog.gotoPage(3);
                }

                // prepopulate the data
                console.log(data);
            }
            this.addDescriptorDialog.show();

        },

        'saveWebPanel': function()
        {
            var obj = {};
             var arr = AJS.$('#wittified-webfrags-webPanel').serializeArray();
             for(var i=0;i<arr.length;i++)
             {
                 obj[arr[i].name]= arr[i].value;
             }


             var data = {};

             var panelLayout = false;
             var globalItems  = ['url','key','location','weight','panel-width','panel-height'];



             for(var i =0; i<globalItems.length;i++)
             {

                 if( globalItems[i].indexOf('panel-')==0)
                 {
                     if(!panelLayout) { panelLayout= {};}
                     panelLayout[globalItems[i].substring(6).toLowerCase()] = AJS.$('#wittified-webfrags-'+globalItems[i]).val();
                 }
                 else
                 {
                     data[globalItems[i]] = AJS.$('#wittified-webfrags-panel-'+globalItems[i]).val();
                 }
             }

             if(panelLayout)
             {
                data.layout = panelLayout;
             }

             data.name = {};
             data.name.value = AJS.$('#wittified-webfrags-panel-name').val();


             var validationMessage = this.validatePanel(data);
             if( validationMessage && validationMessage!='' )
             {
                 AJS.$('#webfrags-panel-dialog-error').html('');
                 AJS.messages.warning("#webfrags-panel-dialog-error", {
                    title: 'Validation error',
                    body: validationMessage
                 });
                 return false;

             }
             else
             {

                 AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/descriptor/panel',
                     'type': 'POST',
                     'contentType': 'application/json',
                     'data': JSON.stringify(  data ),
                     'success':function()
                     {
                         window.location.reload();
                     }
                 })
             }
             return true;
         },
        'saveAdminPage': function()
        {
            var obj = {};
             var arr = AJS.$('#wittified-webfrags-adminItem').serializeArray();
             for(var i=0;i<arr.length;i++)
             {
                 obj[arr[i].name]= arr[i].value;
             }


             var data = {};

             var iconSettings = false;
             var globalItems  = ['url','iconUrl','iconHeight','iconWidth','key','location','weight'];

             for(var i =0; i<globalItems.length;i++)
             {

                 if( globalItems[i].indexOf('icon')==0)
                 {
                     if(!iconSettings) { iconSettings= {};}
                     iconSettings[globalItems[i].substring(4).toLowerCase()] = AJS.$('#wittified-webfrags-admin-'+globalItems[i]).val();
                 }
                 else
                 {
                     data[globalItems[i]] = AJS.$('#wittified-webfrags-admin-'+globalItems[i]).val();
                 }
             }
             if( iconSettings.url)
             {
                 data.icon = iconSettings;
             }
             data.name = {};
             data.name.value = AJS.$('#wittified-webfrags-admin-name').val();

             var validationMessage = this.validateAdmin(data);
             if( validationMessage && validationMessage!='' )
             {
                 AJS.$('#webfrags-admin-dialog-error').html('');
                 AJS.messages.warning("#webfrags-admin-dialog-error", {
                    title: 'Validation error',
                    body: validationMessage
                 });
                 return false;

             }
             else
             {

                 AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/descriptor/admin',
                     'type': 'POST',
                     'contentType': 'application/json',
                     'data': JSON.stringify(  data ),
                     'success':function()
                     {
                         window.location.reload();
                     }
                 })
             }
             return true;
         },
         'validatePanel': function(data)
         {
            var returnMessage = [];
            console.log(data);


            if(!data.location) { returnMessage.push('Location is required');}
            if(!data.name.value) { returnMessage.push('Label is required');}
            if(!data.key) { returnMessage.push('Key is required');}
            if(!data.url) { returnMessage.push('URL is required');}
            if(data.layout && data.layout.width && !data.layout.height) { returnMessage.push('Width specified but no height');}
            if(data.layout && data.layout.height && !data.layout.width) { returnMessage.push('Height specified but no width');}

            return returnMessage.join('<br/>');
        },
         'validateAdmin': function(data)
         {
             var returnMessage = [];
             console.log(data);

             if(!data.location) { returnMessage.push('Location is required');}
             if(!data.name.value) { returnMessage.push('Link Text is required');}
             if(!data.key) { returnMessage.push('Key is required');}
             if(!data.url) { returnMessage.push('URL is required');}
             if(data.icon && data.icon.width && !data.icon.height) { returnMessage.push('Width specified for the icon but no height');}
             if(data.icon && data.icon.height && !data.icon.width) { returnMessage.push('Height specified for the icon but no width');}

             return returnMessage.join('<br/>');
         },
        'saveGeneralPage': function()
        {
            var obj = {};
             var arr = AJS.$('#wittified-webfrags-generalItem').serializeArray();
             for(var i=0;i<arr.length;i++)
             {
                 obj[arr[i].name]= arr[i].value;
             }


             var data = {};

             var iconSettings = false;
             var globalItems  = ['url','iconUrl','iconHeight','iconWidth','key','location','weight'];

             for(var i =0; i<globalItems.length;i++)
             {

                 if( globalItems[i].indexOf('icon')==0)
                 {
                     if(!iconSettings) { iconSettings= {};}
                     iconSettings[globalItems[i].substring(4).toLowerCase()] = AJS.$('#wittified-webfrags-general-'+globalItems[i]).val();
                 }
                 else
                 {
                     data[globalItems[i]] = AJS.$('#wittified-webfrags-general-'+globalItems[i]).val();
                 }
             }
             if( iconSettings.url)
             {
                 data.icon = iconSettings;
             }
             data.name = {};
             data.name.value = AJS.$('#wittified-webfrags-general-name').val();

             var validationMessage = this.validateGeneralPage(data);
             if( validationMessage && validationMessage!='' )
             {
                 AJS.$('#webfrags-general-dialog-error').html('');
                 AJS.messages.warning("#webfrags-general-dialog-error", {
                    title: 'Validation error',
                    body: validationMessage
                 });
                 return false;

             }
             else
             {
             console.log('saving');

                 AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/descriptor/general',
                     'type': 'POST',
                     'contentType': 'application/json',
                     'data': JSON.stringify(  data ),
                     'success':function()
                     {
                         window.location.reload();
                     }
                 })
             }
             return true;
         },
         'validateGeneralPage': function(data)
         {
             var returnMessage = [];
             console.log(data);

             if(!data.location) { returnMessage.push('Location is required');}
             if(!data.name.value) { returnMessage.push('Link Text is required');}
             if(!data.key) { returnMessage.push('Key is required');}
             if(!data.url) { returnMessage.push('URL is required');}
             if(data.icon && data.icon.width && !data.icon.height) { returnMessage.push('Width specified for the icon but no height');}
             if(data.icon && data.icon.height && !data.icon.width) { returnMessage.push('Height specified for the icon but no width');}

console.log('returning' + returnMessage);
             return returnMessage.join('<br/>');
         },
        'saveWebItem': function()
        {
            var obj = {};
            var arr = AJS.$('#wittified-webfrags-webItem').serializeArray();
            for(var i=0;i<arr.length;i++)
            {
                obj[arr[i].name]= arr[i].value;
            }


            var data = {
                target:{
                    type:'page'
                }
            };

            var iconSettings = false;
            var globalItems  = ['url','styleClasses','iconUrl','iconHeight','iconWidth','key','location','weight','target'];

            for(var i =0; i<globalItems.length;i++)
            {


                if( globalItems[i].indexOf('icon')==0)
                {
                    if(!iconSettings) { iconSettings= {};}
                    iconSettings[globalItems[i].substring(4).toLowerCase()] = AJS.$('#wittified-webfrags-'+globalItems[i]).val();
                }
                else if( globalItems[i]=='target')
                {
                        data.target.type = AJS.$('#wittified-webfrags-'+globalItems[i]).val();
                }
                else if( globalItems[i]=='styleClasses')
                {
                        data.target.type = AJS.$('#wittified-webfrags-'+globalItems[i]).val().split(" ");
                }
                else
                {
                    data[globalItems[i]] = AJS.$('#wittified-webfrags-'+globalItems[i]).val();
                }
            }
            if( iconSettings.url)
            {
                data.icon = iconSettings;
            }
            data.name = {};
            data.name.value = AJS.$('#wittified-webfrags-name').val();
            data.target = { 'type':'page'};

            if(AJS.$('#wittified-webfrags-target').val()=='inlinedialog')
            {
                var items = [
                                'offsetY',
                                'isRelativeToMouse',
                                'closeOthers', 
                                'onTop',
                                'persistent',
                                'showDelay',
                                'width',
                                'offsetX',
                                'offsetY'];


                var options = {}
                for(var i =0; i<items.length;i++)
                {
                    var val = AJS.$('#wittified-webfrags-inlinedialog-'+items[i]).val();
                    if(val)
                    {
                        options[items[i]] = val;
                    }

                }
                data.target.type='inlinedialog';
                data.target.options = options;
            }
            else if(AJS.$('#wittified-webfrags-target').val()=='dialog')
            {
                var items = ['height',
                            'width',
                            'chrome'];

                var options = {}
                for(var i =0; i<items.length;i++)
                {
                    var val = AJS.$('#wittified-webfrags-dialog-'+items[i]).val();
                    if( val)
                    {
                        options[items[i]] = AJS.$('#wittified-webfrags-dialog-'+items[i]).val();
                    }

                }

                data.target.type='dialog';
                data.target.options = options;
            }



            var validationMessage = this.validateWebItem(data);
            if( validationMessage && validationMessage!='' )
            {
                AJS.$('#webfrags-dialog-error').html('');
                AJS.messages.warning("#webfrags-dialog-error", {
                   title: 'Validation error',
                   body: validationMessage
                });
                return false;

            }
            else
            {

                AJS.$.ajax( {'url':AJS.contextPath()+'/rest/wittified/webfrags-finder/1.0/descriptor/item',
                    'type': 'POST',
                    'contentType': 'application/json',
                    'data': JSON.stringify(  data ),
                    'success':function()
                    {
                        window.location.reload();
                    }
                })
            }
            return true;
        },
        'validateWebItem': function(data)
        {
            var returnMessage = [];
            console.log(data);

            if(!data.location) { returnMessage.push('Location is required');}
            if(!data.name.value) { returnMessage.push('Link Text is required');}
            if(!data.key) { returnMessage.push('Key is required');}
            if(!data.url) { returnMessage.push('URL is required');}
            if(data.icon && data.icon.width && !data.icon.height) { returnMessage.push('Width specified for the icon but no height');}
            if(data.icon && data.icon.height && !data.icon.width) { returnMessage.push('Height specified for the icon but no width');}

            if( data.target && data.target.type=='dialog')
            {
                if( data.target.options.width && !data.target.options.height)
                {
                    returnMessage.push('Dialog width specified but no height');
                }
                if( data.target.options.height && !data.target.options.width)
                {
                    returnMessage.push('Dialog height specified but no width');
                }
            }
            if( data.target && data.target.type=='inlinedialog')
            {

            }

            return returnMessage.join('<br/>');
        }
    };
}

AJS.toInit(function()
{
    AJS.$('.wittified-wf-click-intercept').live('click', function(evt)
    {
        var item = AJS.$(this);

        if(item.data('letthrough'))
        {
            item.removeData( 'letthrough')
            if( item.attr('href'))
            {
                window.location = item.attr('href');
            }
        }
        else
        {
            evt.preventDefault();
            evt.stopPropagation
            window.wittified.webfragsDescriptor.selectAction( AJS.$(this).data('editkey'));
        }


    });

    AJS.$.get( AJS.contextPath()+ '/rest/wittified/webfrags-finder/1.0/descriptor/keys', function(keys)
    {
        for( var i=0;i<keys.length;i++)
        {
            var item = AJS.$('#'+ keys[i]);


            item.addClass('wittified-wf-click-intercept');
            item.data('editkey', keys[i]);
        }
    })
});